namespace :optimizeplayer do
  namespace :projects do
    desc "Uploads embed codes of all projects to embed storage"
    task :upload_embeds => :environment do
      projects = Project.all
      progress = ProgressBar.create title: 'Processing projects', total: projects.count, format: '%t...(%p%%) |%B|'
      projects.each do |project|
        project.touch
        project.upload_embed
        progress.increment
      end
    end
  end
end
