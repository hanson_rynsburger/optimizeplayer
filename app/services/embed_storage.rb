class EmbedStorage
  attr_reader :cid

  def initialize(project, key = "")
    @cid         = project.cid
    @project     = project
    @key = key
    @credentials = { provider: 'aws', aws_access_key_id: Yetting.aws_access_key_id, aws_secret_access_key: Yetting.aws_secret_access_key }
    @s3_connection  = Fog::Storage.new(@credentials)
  end

  def upload
    directory = @s3_connection.directories.new(
      key: "#{Yetting.bucket_name}/embeds/#{@project.account_id}",
      public: true
    )

    directory.files.create(
      key:    "#{@cid}#{@project.updated_at.to_i}#{@key}.js",
      body:   render,
      public: true
    )
  end

  def render(options = {})

    view_class ||= Class.new ActionView::Base do
      include Rails.application.routes.url_helpers
    end

    view = view_class.new ActionController::Base.view_paths, {project: @project, key: @key}

    view.extend ProjectsHelper

    Uglifier.compile(view.render(template: 'projects/show'))
  end

end