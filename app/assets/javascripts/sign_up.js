//=require jquery
//=require lib/vendor/jquery.prettycheckable
$(function() {
  $('input.pretty').prettyCheckable();
  $(".plan a").click(function(e) {
    e.preventDefault();
    id = $(this).attr("href");
    $(".tab-content, .plan").removeClass('active');
    $(id).addClass('active');
    $(this).parent().addClass("active");
    return false;
  })
})
