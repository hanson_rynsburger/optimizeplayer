angular.module "optimizePlayer", ["ngResource", "ngSanitize", "angularTree", "infinite-scroll", "ui.bootstrap.modal", "ui.bootstrap.tabs", "angular-redactor"]
angular.module("optimizePlayer").config ["$httpProvider", ($httpProvider) ->
  $httpProvider.defaults.headers.common["X-CSRF-Token"] = $("meta[name=csrf-token]").attr("content")
]
angular.module("optimizePlayer").config ["$routeProvider", ($routeProvider) ->
  $routeProvider.when "/csns",
    templateUrl: "/csns.html"
    controller: "CsnCtrl"
]