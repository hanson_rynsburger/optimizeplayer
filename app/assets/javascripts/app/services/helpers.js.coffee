angular.module('optimizePlayer').factory 'Helpers', [() ->
  millisecondsToTimeString: (s) ->
    ms = s % 1000;
    s = (s - ms) / 1000;
    secs = s % 60;
    s = (s - secs) / 60;
    mins = s % 60;
    return mins + ':' + secs;
]
