angular.module('optimizePlayer').factory 'CtaStore', ['$http', '$q', ($http, $q) ->
  deferred = $q.defer()

  currentCta: false
  splittedCuepoints: (cta) ->
    splitted_cuepoints = []
    cta = this.currentCta if !cta
    if cta
      _cuepoints = cta.cuepoints.slice(0)
      while _cuepoints.length > 0
        splitted_cuepoints.push(_cuepoints.splice(0,2))
    splitted_cuepoints

  get: (project_cid) ->
    that = this 
    $http.get("/api/v1/projects/#{project_cid}/ctas")
    .success (data) ->
      angular.extend that, data
      deferred.resolve data
    that.promise = deferred.promise

  all: ->
    ctas = []
    ctas = ctas.concat(this.buttons)
    ctas = ctas.concat(this.htmls)
    ctas.push(this.optin)
    ctas

  getPosition: (cta) -> #FIXME
    switch cta.type 
      when 'CtaButton'
        this.buttons.indexOf(cta) + 1
      when 'CtaHtml'
        this.htmls.indexOf(cta) + 1
      when 'CtaOptin'
        ''

  delete: (cta) ->
    switch cta.type 
      when 'CtaButton'
        this.buttons.splice(this.buttons.indexOf(cta), 1)
      when 'CtaHtml'
        this.htmls.splice(this.htmls.indexOf(cta), 1)
      when 'CtaOptin'
        this.optin = false
]
