angular.module('optimizePlayer').factory 'Project', ['$resource', ($resource) ->
  res = $resource '/api/v1/projects/:id', id: '@id',
    update:
      method: 'PUT'
    setPosition:
      method: 'PUT'
      url:    '/api/v1/projects/:id/set_position'
    toggleFavorite:
      method: 'PUT'
      url:    '/api/v1/projects/:id/toggle_favorite'
    toggleDashboard:
      method: 'PUT'
      url:    '/api/v1/projects/:id/toggle_dashboard'
    move:
      method: 'PUT'
      url:    '/api/v1/projects/move'

  res.prototype.embedCode = ->
    "<script src=\"#{window.location.origin}/projects/#{@cid}.js\"></script>"

  res.prototype.getWidth = ->
    @dimensions.split('x')[0]

  res.prototype.getHeight = ->
    @dimensions.split('x')[1]

  res
]
