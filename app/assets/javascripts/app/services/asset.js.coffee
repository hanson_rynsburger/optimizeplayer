angular.module("optimizePlayer").factory "Asset", ["$resource", "$q", ($resource, $q) ->
  res = $resource "/api/v1/assets/:id", id: "@id"

  res.prototype.copyToCloudfront = (callback) ->
    $.getJSON "/api/v1/assets/#{this.id}/copy_to_cloudfront", (data) =>
      this.remote_url = data.remote_url
      callback()

  return res
]
