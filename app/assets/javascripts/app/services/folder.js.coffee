angular.module('optimizePlayer').factory 'Folder', ['$resource', ($resource) ->
  $resource('/api/v1/folders/:id', id: '@id')
] 
