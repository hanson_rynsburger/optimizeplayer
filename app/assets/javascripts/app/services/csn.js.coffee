angular.module("optimizePlayer").factory "Csn", ["$resource", "$cacheFactory", '$rootScope', ($resource, $cacheFactory, $rootScope) ->
  res = $resource("/api/v1/csns/:id",
    id: "@id"
  ,
    update:
      method: "PUT"
  )
  cache = $cacheFactory("csnCache")
  hasAnyCsn = false # if we don't know - decide it as false
  res.clearCache = ->
    cache.removeAll()

  res.oldQuery = res.query
  res.query = (f) ->
    result = cache.get("query")
    unless result
      result = res.oldQuery(f)
      cache.put "query", result
    else
      f result  if f
    if result.length > 0
      hasAnyCsn = true
    else
      hasAnyCsn = false
    result

  res.hasAny = ->
    hasAnyCsn

  res
]