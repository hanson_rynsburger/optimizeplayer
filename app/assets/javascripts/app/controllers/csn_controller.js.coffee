angular.module("optimizePlayer").controller "CsnCtrl", ["$scope", "Csn", '$rootScope', ($scope, Csn, $rootScope) ->
  
  $("#err").html ""

  onRemove = (res, status) ->
    Csn.clearCache()
    $scope.csns = Csn.query()
    $scope.csn = null

  onSave = (res, status) ->
    Csn.clearCache()
    csn = new Csn(res)
    $scope.csns.push new Csn(res)
    $scope.csn = null
    $scope.isSaving = false

  onUpdate = (res, status) ->
    Csn.clearCache()
    csn = new Csn(res)
    $scope.csn = null
    $scope.isSaving = false

  onError = (err, status) ->
    $("#err").html err.data if err
    $scope.isSaving = false

  $scope.view = (csn) ->
    $("#err").html ""
    $scope.csn = csn

  $scope.removeCsn = (csn) ->
    csn.$remove onRemove, onError if confirm("Are you sure to delete?")

  $scope.save = ->
    func = (if !!$scope.csn.id then "$update" else "$save")
    $scope.isSaving = true
    (if !!$scope.csn.id then $scope.csn[func](onUpdate, onError) else $scope.csn[func](onSave, onError))
]

