angular.module('optimizePlayer').controller('NewsShowsCtrl',['$scope', '$http','$routeParams',function($scope, $http, $routeParams){
    $http.get('/news/'+$routeParams.id+'.json').success(function(data) {
        $scope.news = data;
    });
}]);
