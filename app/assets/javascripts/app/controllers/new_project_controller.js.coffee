angular.module("optimizePlayer").controller "NewProjectCtrl", ["$scope", "$rootScope", "Project", "Csn", "$http", 'Asset', '$filter',
  ($scope, $rootScope, Project, Csn, $http, Asset, $filter) ->

    unless $rootScope.opts && $rootScope.csns[0]
      $rootScope.opts =
        csnForUpload: null
        cloudfront: null

      $rootScope.csns = Csn.query ->
        $rootScope.opts.csnForUpload = $rootScope.csns[0]
        $rootScope.opts.cloudfront = false
        $rootScope.pingDropkick = 'pinged'
        $rootScope.awsCsnIds = $rootScope.csns.filter( (x) -> x.use_cloudfront).map('id')
        $rootScope.csn = $rootScope.csns[0]

    $scope.project = new Project(
      allow_embed: "true"
      allow_pause: "true"
      allowed_urls: ""
      analytics: ""
      asset_ids: []
      auto_start: "no_autoplay"
      dimensions: "720x405"
      display_time: "true"
      display_volume: "true"
      full_screen: "true"
      license_key: ""
      loop: "false"
      play_button: "true"
      scaling: "orig"
      facebook_share: "true"
      twitter_share: "true"
      skin: "skin0"
      tags: ""
      skinColor: "68,153,17"
      align: "none"
      big_play_button: "true"
      control_bar_hide_method: "fade"
      cb_gap: 4,
      title: "New Project"
      show_banner: "true"
      affiliate_link: ""
    )

    $scope.asset = new Asset(
      media_type: "video"
      file_origin: "upload"
      original_filename: false
      needs_encoding: "false"
    )

    $scope.assetProcessed = false

    $scope.sourceType = "aws"

    $scope.$watch 'csns', ->
      if $scope.csns.length > 0
        $scope.csn = false
        $scope.checkCsns()
      else
        $scope.csn = new Csn(active: true, provider: 'aws')
    , true

    $scope.$watch 'sourceType', (val) ->
      $scope.checkCsns

    $scope.checkCsns = ->
      if $filter('filter')($scope.csns, $scope.sourceType).length == 0
        $scope.csn = new Csn(active: true, provider: $scope.sourceType)

    $scope.addCsn = ->
      $scope.csn = new Csn(active: true, provider: $scope.sourceType)

    $scope.updateProject = ->
      if $scope.opts.cloudfront # fixme
        $scope.asset.copyToCloudfront ->
          $scope.project.asset_ids.push($scope.asset.id)
          $scope.project.url = $scope.asset.remote_url
          $scope.$apply()
      else
        $scope.project.asset_ids.push($scope.asset.id)
        $scope.project.url = $scope.asset.remote_url

    $scope.createProject = (callback) ->
      $scope.saveProject ->
        window.location = "/projects/" + $scope.project.cid + "/edit"

    $scope.saveProject = (callback) ->
      $scope.project.$save (data) ->
        $scope.isSaving = false
        window.history.pushState null, null, "/projects/" + $scope.project.cid + "/edit"
        callback() unless callback is `undefined`

]