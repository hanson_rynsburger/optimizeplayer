angular.module('optimizePlayer').controller('GuideStepsCtrl',['$scope', '$http',function($scope, $http){
    $scope.tasks = [];

    $http.get('/getting_started').success( function (data) {
    $scope.tasks = [];
    $scope.tasks = data;
    $scope.selected = $scope.tasks[0];
    });


    $scope.setTask = function(task) {
      $scope.selected = task;

    }
    $scope.isSelected = function (task) {
      return $scope.selected === task;
    }

    $scope.finishTask = function (task) {
      $http.delete('/getting_started/' + task.id).success( function () {
        var index = $scope.tasks.indexOf(task);
        $scope.tasks.splice(index, 1);
        $scope.selected = $scope.tasks[0];
      });
      }

    $scope.visible = function (){
      if($scope.tasks.length > 0) return true;
    }

      }]);
