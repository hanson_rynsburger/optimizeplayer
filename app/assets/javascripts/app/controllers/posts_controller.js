angular.module('optimizePlayer').controller('PostsCtrl',['$scope', '$http',
function ($scope, $http) {
    var months = [ "January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December" ];
    $scope.posts = [];
    $scope.busy = false;
    $scope.page = 1;

    function unique(arrayName)
            {
                var newArray=new Array();
                label:for(var i=0; i<arrayName.length;i++ )
                {
                    for(var j=0; j<newArray.length;j++ )
                    {
                        if(newArray[j]==arrayName[i])
                            continue label;
                    }
                    newArray[newArray.length] = arrayName[i];
                }
                return newArray;
            }
    $scope.pushData = function (data) {
        for (i in data){
            post_date =  new Date(data[i].created_at);
            data[i].month = months[ post_date.getMonth()];
            data[i].year = post_date.getFullYear();
            data[i].day = post_date.getDate();
        }
            p_list = $scope.posts.concat(data);
            $scope.posts = unique(p_list);
    }

    $scope.loadMore = function() {
        // lock server request for 1 second
        if(!$scope.busy) {
            console.log('busy');
            $scope.busy = true;
            loadPosts();
            $scope.page++;
        }
    }

    loadPosts = function() {
        $http.get('/news.json?page=' + $scope.page).success( function (data) {
            if(!!data[0]){
                $scope.pushData(data);
                $scope.busy = false;
            }
        });
    }
}
]);


