angular.module('optimizePlayer').controller 'DashboardCtrl', ['$scope', 'Project', ($scope, Project) ->
  $scope.selectedIds = []
  $scope.tooltips   = {filesId: null, tagsId: null, tab: 1}
  $scope.favorites  = Project.query(dashboard: true)
  $scope.recent     = Project.query(recent: true)
  $scope.newest     = Project.query(newest: true)

  $scope.$watch ->
    $scope.tooltips.tab
  , ->
    $scope.tooltips.filesId = null
    $scope.tooltips.tagsId  = null

  $scope.refreshFavorites = ->
    tmp = Project.query dashboard: true, ->
      $scope.favorites = tmp

  $scope.refreshRecent = ->
    tmp = Project.query recent: true, ->
      $scope.recent = tmp

  $scope.refreshNewest = ->
    tmp = Project.query newest: true, ->
      $scope.newest = tmp
]
