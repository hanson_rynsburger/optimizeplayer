angular.module("optimizePlayer").controller "ProjectCtrl", ["$scope", "$rootScope", "Project", "Asset", "Csn", "CtaStore", "$http", "$compile", "SkinsStore"
  ($scope, $rootScope, Project, Asset, Csn, CtaStore, $http, $compile, SkinsStore) ->

    unless $rootScope.opts && $rootScope.csns[0]
      $rootScope.opts =
        csnForUpload: null
        cloudfront: null

      $rootScope.csns = Csn.query ->
        $rootScope.opts.csnForUpload = $rootScope.csns[0]
        $rootScope.opts.cloudfront = false
        $rootScope.pingDropkick = 'pinged'
        $rootScope.awsCsnIds = $rootScope.csns.filter( (x) -> x.use_cloudfront).map('id')
        $rootScope.csn = $rootScope.csns[0]
    
    $scope.logo = new Asset(
      media_type: "$scope.logo"
      file_origin: "upload"
    )

    $scope.auto_start_options = [
      {"id":'no_autoplay',"name":"No Autoplay"},
      {"id":'autoplay_on_view',"name":"Autoplay On View"},
      {"id":'autoplay',"name":"Autoplay"}
    ];
    
    $scope.control_bar_hide_method_options = [
      {"id":'hide',"name":"Hide"},
      {"id":'fade',"name":"Mouseover"},
      {"id":'drop',"name":"Minimal"},
      {"id":'never',"name":"Do Nothing"}
    ];

    $scope.skins = SkinsStore.skins

    $scope.isModified = false

    $scope.project = new Project(
      allow_embed: "true"
      allow_pause: "true"
      allowed_urls: ""
      analytics: ""
      asset_ids: []
      auto_start: "no_autoplay"
      dimensions: "720x405"
      display_time: "true"
      display_volume: "true"
      full_screen: "true"
      license_key: ""
      loop: "false"
      play_button: "true"
      scaling: "orig"
      facebook_share: "true"
      twitter_share: "true"
      skin: "skin0"
      tags: ""
      skinColor: "68,153,17"
      align: "none"
      big_play_button: "true"
      control_bar_hide_method: "fade"
      cb_gap: 4,
      title: "New Project"
      show_banner: "true"
      affiliate_link: ""
    )

    $scope.assetProcessed = false
    
    $scope.logo = {top: 0, left: 0, orig: false}

    $scope.init = (attrs) ->
      $scope.project = new Project(attrs)
      $scope.setAutoStartOption($scope.project.auto_start)
      $scope.setControlBarHideMethodOption($scope.project.control_bar_hide_method)
      $scope.cta_store = CtaStore
      $scope.cta_store.get($scope.project.cid)
      $scope.logo.top = $scope.project.logo_pos_top
      $scope.logo.left = $scope.project.logo_pos_left

    $scope.createProject = (callback) ->
      $scope.saveProject ->
        window.location = "/projects/" + $scope.project.cid + "/edit"

    $scope.saveProject = (callback) ->
      $scope.project.tags = angular.element("input:hidden[name=\"hidden-tags\"]").val()
      $scope.project.allowed_urls = angular.element("input:hidden[name=\"hidden-urls\"]").val()
      $scope.isSaving = true
      $scope.isModified = false
      if ($scope.project.cid)
        $scope.project.$update () -> 
          $scope.isSaving = false
      else
        $scope.project.$save (data) ->
          $scope.isSaving = false
          window.history.pushState null, null, "/projects/" + $scope.project.cid + "/edit"
          callback() unless callback is `undefined`

    $scope.setSkin = (skin) ->
      $scope.project.skin = skin.id
      $scope.project.skinColor = skin.color
      for item in $scope.skins
        item.selected = false
      skin.selected = true

    $scope.setModified = ->
      $scope.isModified = true
      
    $scope.setAutoStartOption = (val) ->
      if val == 'no_autoplay'
        $scope.auto_start_option = $scope.auto_start_options[0]
      if val == 'autoplay_on_view'
        $scope.auto_start_option = $scope.auto_start_options[1]
      if val == 'autoplay'
        $scope.auto_start_option = $scope.auto_start_options[2]
    $scope.setAutoStart = (val) ->
      $scope.project.auto_start = val.id
      
    $scope.setControlBarHideMethodOption = (val) ->
      if val == 'hide'
        $scope.control_bar_hide_method_option = $scope.control_bar_hide_method_options[0]
      if val == 'fade'
        $scope.control_bar_hide_method_option = $scope.control_bar_hide_method_options[1]
      if val == 'drop'
        $scope.control_bar_hide_method_option = $scope.control_bar_hide_method_options[2]
      if val == 'never'
        $scope.control_bar_hide_method_option = $scope.control_bar_hide_method_options[3]
        
    $scope.setControlBarHideMethod = (val) ->
      $scope.project.control_bar_hide_method = val.id

    $scope.embed_code = ->
      $scope.project.embedCode()

    $scope.setDimensions = (dimensions) ->
      $scope.project.dimensions = dimensions

]