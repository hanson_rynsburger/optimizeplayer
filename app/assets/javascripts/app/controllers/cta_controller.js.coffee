angular.module('optimizePlayer').controller 'CtaCtrl', ['$scope', '$http', 'CtaStore', ($scope, $http, CtaStore) -> 
  $scope.cta_store = CtaStore

  $scope.$watch 'cta_store.currentCta', ->
    $scope.player.pause() if $scope.player

  $scope.setCta = (cta) ->
    $scope.cta_store.currentCta = cta

  $scope.saveCta = ->
    if $scope.cta_store.currentCta.id
      $http.put("/api/v1/projects/#{$scope.project.cid}/ctas/#{$scope.cta_store.currentCta.id}", {cta: $scope.cta_store.currentCta})
      .success ->
        $scope.cta_store.currentCta = false
        $scope.initPlayer()
    else
      $http.post("/api/v1/projects/#{$scope.project.cid}/ctas", {cta: $scope.cta_store.currentCta})
      .success ->
        $scope.cta_store.currentCta = false
        $scope.initPlayer()

  $scope.cancel = ->
    if !$scope.cta_store.currentCta.id
      CtaStore.delete($scope.cta_store.currentCta)
    $scope.cta_store.currentCta = false

  $scope.deleteCta = (cta) ->
    CtaStore.delete(cta)
    $http.delete("/api/v1/projects/#{$scope.project.cid}/ctas/#{cta.id}")
]
