angular.module('optimizePlayer').controller 'CtasCtrl', ['$scope', '$http', 'CtaStore', ($scope, $http, CtaStore) -> 
  
  $scope.cta_store = CtaStore

  $scope.ctaTemplate = {
        background_color: "#2bc3e3",
        font_family: "Open Sans",
        font_style: "300",
        font_size: "26px",
        text_color: "#ffffff",
        cuepoints: [],
        on_start: "false",
        on_pause: "false",
        on_finish: "false",
        on_cuepoints: "false",
        background_opacity: "1",
        text_opacity: "1",
        fullscreen: "true",
        location: "inside",
        text: "Play Video",
        link: "http://google.com",
        position: "inside",
      }
  
  $scope.addCta = (type) ->
    $scope.cta_store.promise.then (data) ->
      switch type
        when 'button'
          $scope.cta_store.currentCta = angular.extend {type: "CtaButton", cid: new Date().getTime()}, $scope.ctaTemplate
          $scope.cta_store.buttons.push($scope.cta_store.currentCta)
        when 'html'
          $scope.cta_store.currentCta = angular.extend {type: "CtaHtml", cid: new Date().getTime()}, $scope.ctaTemplate
          $scope.cta_store.htmls.push($scope.cta_store.currentCta)
        when 'optin'
          $scope.cta_store.currentCta = angular.extend {
                                                          button_background_color: "#2bc3e3", 
                                                          type: "CtaOptin", 
                                                          header_text: "Enter your email to play this video"
                                                          cid: new Date().getTime(), 
                                                          skip: "true"}, $scope.ctaTemplate
          $scope.cta_store.optin = $scope.cta_store.currentCta
]
