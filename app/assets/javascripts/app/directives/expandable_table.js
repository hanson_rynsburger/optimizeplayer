'use strict';

angular.module('optimizePlayer').directive('expandableTable', function() {
  return {
    link: function($scope, $element, $attrs) {
       $element.on('click', '.download a', function() {
        $(this).remove();
        $element.find('.hidden').removeClass('hidden');
       })
    }
  }
});
