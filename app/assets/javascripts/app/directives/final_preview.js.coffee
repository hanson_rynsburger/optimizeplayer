"use strict"
angular.module("optimizePlayer").directive "finalPreview", ["SkinsStore", '$timeout', 'CtaStore', (SkinsStore, $timeout, CtaStore) ->
  link: ($scope, $element, $attrs) ->

    $scope.$watch 'project', ->
      if $scope.player and $scope.player.isLoaded()
        $scope.setModified()
    , true
    
    $scope.$watch 'project.dimensions', (val) ->
      if $scope.player and $scope.player.isLoaded()
        width = $scope.project.getWidth()
        height = $scope.project.getHeight()
        $('#player' + short_cid + ' .cta').css('max-width', width + 'px')
        $('#player' + short_cid + ' .cta').css('max-height', height + 'px')
        $('#player' + short_cid + ' .cta.outside .optin').css('width', width + 'px')
        $('#player' + short_cid + ' .cta.outside .optin').css('height', height + 'px')
        $('#player_cont_' + short_cid).css('width', width + 'px')
        $('#player_cont_' + short_cid).css('height', height + 'px')
        $scope.setMaxWidth()
        $scope.setMaxHeight()
    
    $scope.$watch "project.max_width", ((value) ->
      $scope.setMaxWidth()
    )

    $scope.$watch "project.max_height", ((value) ->
      $scope.setMaxHeight()
    )

    $scope.$watch "step", (val) ->
      $scope.initPlayer()  if val is 3
    
    $scope.$watch 'project.full_screen', (val) ->
      $scope.player_skin_controls.displayFullscreen $scope.isTrue(val) if $scope.player and $scope.player.isLoaded()

    $scope.$watch 'project.play_button', (val) ->
      $scope.player_skin_controls.displaySmallPlay $scope.isTrue(val) if $scope.player and $scope.player.isLoaded()

    $scope.$watch 'project.big_play_button', (val) ->
      $scope.player_skin_controls.displayBigPlay $scope.isTrue(val) if $scope.player and $scope.player.isLoaded()

    $scope.$watch 'project.display_time', (val) ->
      $scope.player_skin_controls.displayTime $scope.isTrue(val) if $scope.player and $scope.player.isLoaded()

    $scope.$watch 'project.mute', (val) ->
      $scope.player_skin_controls.allowMute $scope.isTrue(val) if $scope.player and $scope.player.isLoaded()

    $scope.$watch 'project.display_volume', (val) ->
      $scope.player_skin_controls.displayVolume $scope.isTrue(val) if $scope.player and $scope.player.isLoaded()

    $scope.$watch 'project.allow_pause', (val) ->
      $scope.player_skin_controls.allowPause $scope.isTrue(val) if $scope.player and $scope.player.isLoaded()

    $scope.$watch 'project.skinColor', (val) ->
      if $scope.player and $scope.player.isLoaded()
        $scope.player_skin_controls.setColor parseInt($scope.playerController.colorConverter.RGBToBin(val.split(',')))

    $scope.$watch 'project.control_bar_hide_method', (val) ->
      if $scope.player and $scope.player.isLoaded()
        if val == 'hide'
          $scope.player_skin_controls.displayControls false
        else
          $scope.player_skin_controls.displayControls true
          $scope.player_skin_controls.setHideMethod val
          
    $scope.$watch 'control_bar_hide_method_option', (val) ->
      $scope.setControlBarHideMethod(val)

    $scope.$watch 'project.cb_gap', (val) ->
      if $scope.player and $scope.player.isLoaded()
        $scope.setModified()
      $scope.player_skin_controls.setGap parseInt(val) if $scope.player and $scope.player.isLoaded()
      
    $scope.$watch 'project.affiliate_link', (val) ->
      if $scope.player and $scope.player.isLoaded()
        $scope.player_skin_controls.setBrandingURL(val)
        
    $scope.$watch 'project.show_banner', (val) ->
      if $scope.player and $scope.player.isLoaded()
        if $scope.isTrue(val)
          $scope.player_skin_controls.setBrandingURL($scope.project.affiliate_link)
        else
          $scope.player_skin_controls.setBrandingURL("")

    $scope.$watch 'project.start_image', (val) ->
      $scope.project.auto_start = "no_autoplay" if val
      
    $scope.$watch 'project.auto_start', (val) ->
      $scope.initPlayer()

    $scope.$watch 'auto_start_option', (val) ->
      $scope.setAutoStart(val)

    $scope.$watch "[project.scaling, project.logo, project.logo_position, project.allow_embed, 
                    project.auto_mute, project.facebook_share, project.twitter_share, project.autoplay, project.loop, project.start_image, project.skin]", (->
      $scope.initPlayer()
    ), true

    $scope.setMaxWidth = ->
      if $scope.project.dimensions is "responsive"
        $('#player_cont_' + short_cid).css('max-width', $scope.project.max_width + 'px')
      else
        $('#player_cont_' + short_cid).css('max-width', '')

    $scope.setMaxHeight = ->
      if $scope.project.dimensions is "responsive"
        $('#player_cont_' + short_cid).css('max-height', $scope.project.max_height + 'px')
      else
        $('#player_cont_' + short_cid).css('max-height', '')

    $scope.isTrue = (val) ->
      val isnt "false" and val isnt false and val isnt `undefined`

    $scope.initPlayer = ->
      $scope.playerController = eval('optimizeplayer_' + $attrs.shortCid)
      $scope.playerController.project = $scope.project
      
      $scope.cta_store.promise.then (data) ->
        $scope.playerController.ctas = $scope.cta_store.all()
        $scope.playerController.embed = false
        $(".cta").hide()
        $scope.playerController.initplayer()
        $scope.player = $scope.playerController.player
        $scope.player_skin_controls = $scope.player.getPlugin("skin_controls")
]