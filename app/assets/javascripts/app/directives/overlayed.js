'use strict';

angular.module('optimizePlayer').directive('overlayed', function() {
  return {
    link: function($scope, $element, $attrs) {
      $element.on('mouseenter', function(){
        $(this).find('.overlay').stop().fadeIn(250);
      }).on('mouseleave', function(){
        $(this).find('.overlay').stop().fadeOut(250);
      });
    }
  }
});
