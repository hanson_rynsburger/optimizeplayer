'use strict';

angular.module('optimizePlayer')
  .directive('flowPlayerModal', function() {
    return {
      link: function($scope, $element) {
        $scope.$watch('file', function(val) {
          if (val && val.url) {
            $element.flowplayer();
          }
        });
      }
    }
  }); 
