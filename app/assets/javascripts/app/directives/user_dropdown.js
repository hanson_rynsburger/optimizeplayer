'use strict';

angular.module('optimizePlayer')
  .directive('userDropdown', function() {
    return {
      link: function($scope, $element, $attrs) {
        var dropdown = $element.find('.user-dropdown')
          , toggl    = $element.find('.open-user-dropdown');

        toggl.click(function() {
          dropdown.stop().slideToggle(250);
          toggl.toggleClass('selected');
          $('body').on('click', function() {
            dropdown.stop().slideToggle(250);
            toggl.toggleClass('selected');
            $('body').off('click');
          });
          return false;
        });
      }
    }
  });
 
