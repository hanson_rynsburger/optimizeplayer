'use strict';

angular.module('optimizePlayer').directive('assetOrigin', function() {
  return {
    link: function($scope, $element) {
      $scope.$watch('asset', function(val) {
        if (val) {
          $element.on('change', ':radio', function(){
            var $radio = $(this);
            $radio.parents('li').addClass('selected').siblings('li.selected').removeClass('selected');
            $scope.$apply(function() {
              $scope.asset.file_origin = $radio.val();
            });
          });
          $element.find(':radio').prettyCheckable();
        }
      });
    }
  }
});
