'use strict';

angular.module('optimizePlayer').directive('logoPosition', function() {
  return {
    link: function($scope, $element, $attrs) {
      var $samples = $('a.logo-sample img', $element);
      $scope.$watch('project.logo', function(val) {
        if (val) 
          $samples.attr('src', $scope.project.logo);
        else
          $samples.attr('src', "/assets/logo-sample.png");
      });
      $samples.parent().click(function() {
        var $link = $(this);
        $link.siblings('a').removeClass('selected');
        $link.addClass('selected');
        $scope.$apply(function() {
          $scope.project.logo_position = $link.attr('data-position');
        });
      });
    }
  }
});
