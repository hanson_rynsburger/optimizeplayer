"use strict"
angular.module("optimizePlayer").directive "optinskipbtn", [ ->

  link: ($scope, $element, $attrs) ->
    $element.bind 'click', ->
      this.parentNode.parentNode.style.display = 'none';
      $scope.player.play();
]