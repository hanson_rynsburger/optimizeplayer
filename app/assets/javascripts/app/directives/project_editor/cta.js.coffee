"use strict"
angular.module("optimizePlayer").directive "cta", ['CtaStore', (CtaStore) ->
  link: ($scope, $element, $attrs) ->
    obj = $scope

    model = $attrs.ngModel.split(".")
    for i in model
      obj = obj[i]
    $scope.obj = obj

    $element.css({
      top: $scope.obj.top,
      left: $scope.obj.left});

    $element
      .draggable({
        containment: ".inside",
        drag: (event, ui) ->
          return false if CtaStore.currentCta.id != $scope.obj.id
        stop: (event, ui) ->
          $parent = $(this).closest('.inside')
          $scope.setPositions($(this))
      })

    $attrs.$observe "resizable", (val) ->
      if val == 'true'
        $element
          .resizable({
            handles: 'ne',
            containment: ".inside",
            minHeight: 117,
            minWidth: 292,
            maxHeight: 500,
            maxWidth: 500
            stop: (event, ui) ->
              $scope.setPositions($(this))
          })
        $scope.isResizable = true
      else if $scope.isResizable
          $element.resizable("destroy")
          $scope.isResizable = false

    $scope.setPositions = ($elem) ->
      $parent = $element.parent()

      $scope.$apply ->
        $scope.obj.left = parseInt($elem.css("left")) + 'px'
        $scope.obj.top = parseInt($elem.css("top")) + 'px'

]