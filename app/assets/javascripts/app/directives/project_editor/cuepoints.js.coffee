angular.module('optimizePlayer').directive 'cuepoints', ['$timeout', ($timeout) ->
  restrict: 'A',
  require: 'ngModel',
  scope: true,
  link: ($scope, $element, $attrs, ctrl) ->


    $scope.$watch "cuepoints", (val) ->
      if val && val.length > 0
        ctrl.$setViewValue($scope.cuepoints.join().split(",").map( (e) -> parseInt(e)))
    , true

    $scope.$watch 'cta_store.currentCta', (cta) ->
      if (typeof ctrl.$viewValue == "string")
        ctrl.$setViewValue(JSON.parse(ctrl.$viewValue))

      $scope.cuepoints = []
      if ctrl.$viewValue
        cuepoints_copy = ctrl.$viewValue.slice()
        while (cuepoints_copy.length > 0)
          $scope.cuepoints.push(cuepoints_copy.splice(0, 2))

    $attrs.$observe "enabled", (val) ->
      if val != 'true' && val != true
        $scope.cuepoints = [] 
        ctrl.$setViewValue([])

    $scope.addCuepoint = ->
      $scope.cuepoints.push([0,0])

]
