angular.module("optimizePlayer").directive "csnFinder", ["$http", "$timeout", ($http, $timeout) ->
  link: ($scope, $element, $attrs) ->

    $scope.$watch "csns", ->
      $element.find(".dropkick").dropkick change: (value) ->
        $scope.$apply ->
          $scope.current_csn = $scope.csns[value]

    $scope.$watch "current_csn", ->
      if $scope.current_csn
        $http.get("/csns/#{$scope.current_csn.id}/directories").success (data) ->
          $scope.current_csn.buckets = data

    $scope.$watch 'sourceType', ->
      $scope.current_csn = false

    $element.on "click", ".main-csn li a.folder", ->
      if $(this).siblings("ul").find("li").length > 0
        $this = $(this)
        $this.siblings("ul").stop().slideToggle 250
      false

    $scope.setCurrentFolder = (folder, isBucket = false) ->
      $scope.current_folder = folder
      if isBucket && !$scope.current_folder.loaded
        $scope.current_folder.isBucket = true
        $http.get("/csns/#{$scope.current_csn.id}/directories", {params: {bucket: folder.name}}).success (data) ->
          angular.extend($scope.current_folder, data)
          $scope.current_folder.loaded = true

    $scope.setConnectedFile = (file) ->
      $.extend $scope.asset,
        csn_id: $scope.current_csn.id
        key: file.key
        original_filename: file.name
        content_type: file.content_type
        remote_url: file.url

    $scope.selectFile = ->
      if $scope.asset.needs_encoding
        $modal = $('#upload-new-video')
        $modal.css("margin-top", "-" + ($modal.height() / 2) + "px")
        $modal.foundation('reveal', 'open')
      else
        $scope.updateProject()
        $scope.createProject()
]