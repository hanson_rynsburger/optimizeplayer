angular.module('optimizePlayer').directive 'timeField', ['Helpers', (Helpers) ->
  restrict: 'A',
  require: 'ngModel',
  link: (scope, element, attr, ngModel) ->

    element.bind "keypress", (e) ->
      char = String.fromCharCode(e.which || e.charCode || e.keyCode)
      if (element.val().match(":").length > 0 && char == ":") || !char.match(/[0-9]|:/g)
        e.preventDefault()

    ngModel.$parsers.push (text) ->
      text = text.replace(/[^\d:]/g, '');
      vals = text.split(":").slice(0,2)

      if !vals[1]
        vals[1] = vals[0]
        vals[0] = 0
      return (vals[0]*60 + parseInt(vals[1]))*1000

    ngModel.$formatters.push (s) ->
      Helpers.millisecondsToTimeString(s)
]