"use strict"
angular.module("optimizePlayer").directive "timeline", ['$window', 'CtaStore', 'Helpers', ($window, CtaStore, Helpers) ->
  link: ($scope, $element, $attrs) ->

    $scope.duration = ""

    angular.element($window).bind 'metaDataLoaded', ->
      $scope.$apply ->
        if ($scope.seconds = $scope.player.getClip().duration) > 0
          $scope.duration = Helpers.millisecondsToTimeString($scope.seconds*1000)
        $scope.gap = parseInt(720 / $scope.seconds)
        $scope.loadCtas()

    $scope.$watch 'cta_store', (buttons) ->
      $scope.loadCtas()
    , true

    $scope.loadCtas = ->
      $scope.cta_store.promise.then (data) ->
        $scope.ctas = $scope.cta_store.all()
]


angular.module('optimizePlayer').directive 'timelineBlock', ['$compile', 'CtaStore', 'Helpers', '$timeout', ($compile, CtaStore, Helpers, $timeout) ->
  replace: true
  link: ($scope, $element, $attrs) ->
    
    $scope.show_tooltip = false

    $scope.render = ->
      
      if $scope.cta
        tmpl = "<div class='tooltip tooltip-cta' ng-mouseenter='show_tooltip=true' ng-mouseleave='show_tooltip=false' ng-show='show_tooltip'>" +
               "<h6>{{cta.type}} {{cta_store.getPosition(cta)}}</h6>"

        $scope.cuepoints = CtaStore.splittedCuepoints($scope.cta)
        tooltip_list = "<ul>"
        timelines = ""

        $scope.cuepoints.map (cuepoint) ->

          tooltip_list += "<li><i class=\"icon-time\"></i> #{Helpers.millisecondsToTimeString(cuepoint[0])} - #{Helpers.millisecondsToTimeString(cuepoint[1])} <i>min</i></li>"

          left = $scope.gap * (cuepoint[0] / 1000)
          width = $scope.gap * (cuepoint[1] / 1000) - left
          center = left + (width / 2)
          timelines += "<div ng-mouseenter='showTooltip(#{center})' ng-mouseleave='hideTooltip()' class='cta-object #{$scope.cta.type}' style='left: #{left}px; width: #{width}px;'><div class='cta-type'></div></div>"

        tooltip_list += "</ul>"
        tmpl += tooltip_list
        tmpl += "</ul><a class='blue button radius flat edit' ng-click='setCta(cta)'>Edit</a><a class='red button radius flat delete' ng-click='deleteCta(cta)'>Delete</a></div>"
        tmpl += timelines

        $element.html(tmpl).show()
        $compile($element.contents())($scope)

        $scope.tooltip = $element.find(".tooltip")

    $scope.showTooltip = (center) ->
      $scope.tooltip.css({left: center - $scope.tooltip.outerWidth() / 2, top: "-#{$scope.tooltip.outerHeight()}px" })
      $scope.show_tooltip = true
      $timeout.cancel($scope.promise)

    $scope.hideTooltip = ->
      $scope.promise = $timeout -> 
                        $scope.show_tooltip = false
                       , 200

    $scope.$watch 'cta.cuepoints', ->
      $scope.render() 

    $scope.$watch 'gap', ->
      $scope.render() if $scope.gap > 0

]

angular.module('optimizePlayer').directive 'timelineTooltip', ['$compile', 'CtaStore', ($compile, CtaStore) ->
  replace: true
  link: ($scope, $element, $attrs) ->
    $scope.cta_store = CtaStore

    $scope.render = ->
      $scope.cuepoints = []
      
      if $scope.cta_store.currentCta.cuepoints
        $scope.cuepoints = CtaStore.splittedCuepoints()
        tmpl = "<ul>"
        $scope.cuepoints.map (cuepoint) ->
          tmpl += "<li>#{cuepoint}</li>"
        tmpl += "</ul>"
        $element.html(tmpl).show()
        $compile($element.contents())($scope)

    $scope.$watch 'cta_store.currentCta', (cta) ->
      $scope.render() if cta

]