angular.module('optimizePlayer').directive 'projectFavorite', ->
  restrict: 'E'
  templateUrl: '/assets/project_favorite.html'
  scope:
    project:  '=project'
    onUpdate: '&onUpdate'
  replace: true
  controller: ['$scope', '$element', ($scope, $element) ->
    $scope.favoriteClass = ->
      if $scope.project.favorite
        'picon-favourite-on'
      else
        'picon-favourite-off'

    $scope.favorToggle = ->
      $scope.project.favorite = not $scope.project.favorite
      $scope.project.$toggleFavorite ->
        if $scope.onUpdate()
          $scope.onUpdate()($scope.project)
  ]
