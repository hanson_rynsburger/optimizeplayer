angular.module('optimizePlayer').directive('ngHtmlRaw', function() {
    return function(scope, element, attrs) {
        scope.$watch(attrs.ngHtmlRaw, function(value) {
            element[0].innerHTML = value;
        });
    }
});


