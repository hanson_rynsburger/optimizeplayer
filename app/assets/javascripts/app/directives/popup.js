'use strict';

angular.module('optimizePlayer').directive('popup', function() {
  return {
    link: function($scope, $element, $attrs) {
			$('.help', $element).on('mouseenter', function(){
				$(this).siblings('.popup-content').stop().fadeIn(250);
				return false;
			}).on('mouseleave', function(){
				$(this).siblings('.popup-content').stop().fadeOut(250);
				return false;
			});
    }
  }
});
