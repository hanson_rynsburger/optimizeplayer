'use strict';

angular.module('optimizePlayer')
  .directive('selectServiceProvider', function() {
    return {
      link: function($scope, $element, $attrs) {
        angular.forEach(['csn', 'encoder'], function(service) {
          $scope.$watch(service, function(val) {
            if (angular.isObject(val)) {
              $element.dropkick({
                change: function(value, label) {
                  $scope.$apply(function() {
                    $scope[service]['provider'] = value;
                  });
                }
              });
            }
          });
        });
      }
    }
  });
