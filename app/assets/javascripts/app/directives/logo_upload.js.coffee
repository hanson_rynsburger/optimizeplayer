angular.module("optimizePlayer").directive "logoUpload", ['Csn', (Csn) ->
  restrict: "A"
  scope: {
    file: '=ngModel',
    setModified: '&setModified'
  }
  require: 'ngModel'
  link: ($scope, $element, $attrs, ctrl) ->

    $scope.removeFile = ->
      $scope.setModified()
      $scope.file = null
      $scope.percentage = "0%"
      $scope.uploadData = null
      $scope.uploadResult = null
      $scope.btnText = "Upload"
      $scope.original_filename = null

    $scope.onProgressChange = (percentage) ->
      $scope.percentage = percentage + "%"

    $scope.onDone = (url) ->
      $scope.setModified()
      $scope.file = url

    $scope.moveLogo = ->
      $("#logo-preview").show()
      $scope.$parent.logo.orig = $scope.$parent.project.logo
      $scope.$parent.project.logo = false
      $scope.$parent.cta_store.currentCta = false
      $scope.isMoving = true

    $scope.saveLogo = ->
      $scope.$parent.project.logo = $scope.$parent.logo.orig
      $scope.$parent.logo.orig = false
      $scope.$parent.project.logo_pos_top = $("#logo-preview img").css("top")
      $scope.$parent.project.logo_pos_left = $("#logo-preview img").css("left")
      $("#logo-preview").hide()
      $scope.isMoving = false
]