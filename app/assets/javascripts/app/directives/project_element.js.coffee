angular.module('optimizePlayer').directive 'projectElement', ->
  restrict: 'E'
  templateUrl: '/assets/project_element.html'
  scope:
    project:      '=project'
    sync:         '=sync'
    selectedIds:  '=selectedIds'
    dashboard:    '=dashboard'
    favoritesResort: '&favoritesResort'
  replace: true
  controller: ['$scope', '$element', '$timeout', 'Project', ($scope, $element, $timeout, Project) ->
    $scope.$on 'dropSelection', ->
      $scope.selected = false

    $scope.clickOnProject = ->
      project = $scope.project
      unless $scope.clickedOP
        $scope.clickedOP = $timeout ->
          $scope.clickedOP = null
          location.href = "/projects/#{project.cid}/edit"
        , 500

    $scope.dblclickOnProject = ->
      project = $scope.project
      $timeout.cancel $scope.clickedOP
      $scope.clickedOP = null
      $scope.nameEditing = true
      $scope.newTitle  = project.title

    $scope.updateProjectName = ->
      project = $scope.project
      $scope.nameEditing = false
      if $scope.newTitle and $scope.newTitle != project.title
        project.title = $scope.newTitle
        Project.update(id: project.id, title: project.title)

    $scope.onFavoriteUpd = $scope.favoritesResort()

    $scope.toggleIdInIds = ->
      if $scope.selected
        $scope.selectedIds.push $scope.project.id
      else
        $scope.selectedIds.remove $scope.project.id
  ]
