'use strict';

angular.module('optimizePlayer')
  .directive('fileDetailsToggle', function() {
    return {
      link: function($scope, $element, $attributes) {
        $element.on('click', function(e) {
          $element.addClass('selected').parent().siblings('li').find('a.file').removeClass('selected');
          return false;
        });
      }
    }
  });
