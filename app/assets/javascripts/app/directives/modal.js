'use strict';

angular.module('optimizePlayer').directive('modal', function() {
  return {
    link: function($scope, $element, $attrs) {
    	$(".close-modal", $element).click(function() {
    		$element.foundation('reveal', 'close');
    	})
    }
  }
});
