'use strict';

angular.module('optimizePlayer')
  .directive('modalSubmit', function() {
    return {
      link: function($scope, $element) {
        $element.on('click', function(e) {
          e.preventDefault();
          $('#modal-csn a.close-reveal-modal').click();
        })
      }
    }
  });
