angular.module('optimizePlayer').directive 'projectDashboard', ->
  restrict: 'E'
  templateUrl: '/assets/project_dashboard.html'
  scope:
    project:  '=project'
    onUpdate: '&onUpdate'
  replace: true
  controller: ['$scope', '$element', ($scope, $element) ->
    $scope.dashboardClass = ->
      if $scope.project.dashboard
        'picon-favourite-on blue'
      else
        'picon-favourite-off'

    $scope.dashboardToggle = ->
      $scope.project.dashboard = not $scope.project.dashboard
      $scope.project.$toggleDashboard ->
        if $scope.onUpdate()
          $scope.onUpdate()($scope.project)
      , -> # 422 code
        alert 'Only 5 projects in the dashboard allowed'
        if $scope.onUpdate()
          $scope.onUpdate()($scope.project)
  ]
