'use strict';

angular.module('optimizePlayer').directive('tabs', function() {
  return {
    restrict: 'E',
    scope: {},
    replace: true,
    transclude: true,
    controller: ['$scope', function($scope, $element) {
      var panes = $scope.panes = [];

      $scope.select = function(pane) {
        angular.forEach(panes, function(pane) {
          pane.selected = false;
        });
        pane.selected = true;
      }

      this.addPane = function(pane) {
        if (!panes.length) $scope.select(pane);
        panes.push(pane);
      }
    }],
    template: '<div>' +
                '<ul class="nav-tabs inline-list clearfix">' +
                  '<li ng-repeat="pane in panes" ng-class="{active:pane.selected}">' +
                    '<a href="" ng-click="select(pane)">{{pane.title}}</a></li>' +
                '</ul>' +
                '<div class="tab-content clearfix" ng-transclude></div>' +
              '</div>'
  }
});


