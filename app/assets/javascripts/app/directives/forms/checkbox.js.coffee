# <span checkbox ng-model="obj.field" label="My field"></span>
angular.module('optimizePlayer').directive 'checkbox', ['$timeout', ($timeout) ->
  restrict: 'A',
  require: 'ngModel',
  scope: {
    value: "=ngModel",
    disabled: "=ngDisabled"
  },
  template: '<div class="checkbox clearfix prettycheckbox labelright blue {{class}}" ng-class="{disabled: disabled}" ng-click="check()">' + 
              '<a></a>' +
              '<label ng-show="label">{{label}}</label>' + 
            '</div>'
  link: (scope, element, attr, ctrl) ->
    scope.label = attr.label
    scope.class = attr.class
    scope.check = ->
      if !scope.disabled
        scope.value = (scope.value == undefined || scope.value.toString() == 'false')
        element[0].querySelector("a").classList.toggle("checked", scope.value)

        if attr.callback
          scope[attr.callback]()

    $timeout ->
      element[0].querySelector("a").classList.toggle("checked", scope.value == 'true')
]