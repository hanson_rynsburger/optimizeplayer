'use strict';

angular.module('optimizePlayer').directive('colorpicker', function() {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function($scope, $element, $attrs, ngModel) {

      $element[0].onclick = function(e) {
        e = e || window.event;
        colorPicker(e);
        colorPicker.cP.style.zIndex = 5000;
        colorPicker.cP.style.left = '-180px';
        colorPicker.exportColor = function() {
          //var str = $attrs['ngModel'] + '="' + $element[0].value + '"';
          //console.log($scope.$eval(str));
          ngModel.$setViewValue($element[0].value);
          $scope.$apply();

        };
      };

      ngModel.$formatters.push(function(s) {
        var rgba, color = {};
        if (typeof s == "string") {
          rgba = s.split(",")
          rgba  = [rgba[0], rgba[1], rgba[2]];
        }
        else 
          rgba = s;
        return $.Color().rgba(rgba).toHexString();
      });

      ngModel.$parsers.push(function(s) {
        var red, green, blue;
        var color = $.Color(s);
        red = color.red();
        green = color.green();
        blue = color.blue();

        return red + ',' + green + ',' + blue
      });
    }
  }
});
