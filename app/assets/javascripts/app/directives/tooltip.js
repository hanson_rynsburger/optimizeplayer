'use strict';

angular.module('optimizePlayer').directive('tooltip', function() {
  return {
    link: function($scope, $element, $attrs) {
      $('.use-tooltip', $element).on('mouseenter', function(){
        var type = $(this).data('tooltip');
        var title = $(this).data('title');
        var tooltip = $('#tooltip-' + type);
        $(this).append(tooltip);
        $(tooltip).find('span').html(title).end().stop().fadeIn(250);
      }).on('mouseleave', function(){
        var tooltip = $(this).data('tooltip');
        $('#tooltip-' + tooltip).stop().fadeOut(250);
      });
    }
  }
});
