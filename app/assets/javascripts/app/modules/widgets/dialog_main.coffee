'use_strict'

angular.module('optimizePlayer')

  .controller("dialogCtrl", ["$scope", "videoPageData", 'dialog', ($scope, videoPageData, dialog) ->

    $scope.editWidget = (widgetName, id = null) ->
      #TODO check if widget is not exist
      dialog.title = "Widget Options"
      dialog.open(
        templateUrl: "/assets/widget_popover_#{widgetName}.html",
        controller: 'widgetsCtrl'
        data:
          id: id
          widgetName: widgetName
      ).then( (data) ->
          console.log "Done"
        )

    $scope.addWidget = (widgetName, id = null)->
      $scope.data = videoPageData.getWidget(id, widgetName)
      videoPageData.saveWidget $scope.data

    $scope.seoForm = ->
      dialog.title = "SEO Options"
      dialog.open(
        templateUrl: "/assets/popover_seo.html",
        controller: 'seoCtrl'
        data: videoPageData.getSeo()
      ).then( (data) ->
          console.log "Done"
        )
  ])


  .controller('widgetsCtrl', [ '$scope', '$modalInstance','videoPageData', 'data', ($scope, $modalInstance, videoPageData, data) ->

    $scope.data = videoPageData.getWidget(data.id, data.widgetName)

    $scope.save = =>
      if _validateData $scope.data
        videoPageData.saveWidget $scope.data
        $modalInstance.close $scope.data

    $scope.cancel = ->
      $modalInstance.dismiss 'cancel'

    _validateData = (data) ->
      if data and data.validation
        for key, validation_rules of data.validation
          # console.log "key is: ", key
          for rule in validation_rules
            # console.log "val is: ", rule
            if rule is "required"
              unless data[key]
                alert "Enter required field(s)"
                return no
      yes

  ])


  .controller('seoCtrl', [ '$scope', '$modalInstance','videoPageData', 'data', ($scope, $modalInstance, videoPageData, data) ->
    sourceData = angular.copy data
    $scope.data = data

    $scope.save = ->
      if _validateData $scope.data
        videoPageData.setSeo $scope.data
        $modalInstance.close $scope.data

    $scope.cancel = ->
      angular.extend $scope.data, sourceData
      $modalInstance.dismiss 'cancel'

    $scope.pull = (keyword) ->
      index = _.indexOf $scope.data.keywords, keyword
      $scope.data.keywords.splice index, 1

    _validateData = (data) ->
      if data and data.validation
        for key, validation_rules of data.validation
          # console.log "key is: ", key
          for rule in validation_rules
            # console.log "val is: ", rule
            if rule is "required"
              unless data[key]
                alert "Enter required field(s)"
                return no
      yes

  ])

  .controller('buttonsWidgetCtrl', [ '$scope','videoPageData', ($scope, videoPageData) ->
    $scope.buttons_widget = videoPageData.getCustomButtons()

    $scope.current = false
    $scope.selectButton = (name)->
      $scope.current = name
      index = _.findIndex($scope.buttons_widget, {name: name})
      if index isnt -1
        $scope.data.button_url = $scope.buttons_widget[index].button_url
        $scope.data.name = $scope.buttons_widget[index].name
  ])


  .factory('dialog', ['$modal','dialogPosition', ($modal, dialogPosition) ->
    dialog =
      options:
        title: ''
      open: (options) ->
        modalInstance = $modal.open
          templateUrl: options.templateUrl,
          controller: options.controller,
          keyboard: no
          resolve:
            data: ->
              options.data
        modalInstance.result
  ])


  .directive( "posXy", [ 'dialogPosition', (dialogPosition) ->
    restrict: 'A'
    replace: true
    link: (scope, element, attrs) ->
      element.on 'click', (e)->
        e.preventDefault()
        pos = $(@).offset()
        window_width = angular.element(window).width()
        window_height = angular.element(window).height()
        if pos.left >  window_width/2
          pos.left -= 300

        if pos.top >= window_height - 100
          pos.top -= 300
        dialogPosition.set pos
        setTimeout ->
          widget_height = angular.element('.custom-modal').height()
          if (pos.top + widget_height) > window_height
            angular.element("body").animate({scrollTop: element.offset().top + widget_height/10}, 'normal');
        ,100

  ])

  .directive "seoKeywords",([ 'videoPageData', (videoPageData) ->
    restrict: 'A'
    replace: true
    priority: 1
    link: (scope, element, attrs) ->
      seoObj = videoPageData.getSeo()

      setNewSeoObj = (obj) ->
        new_keyword = $.trim obj.val()
        new_keyword = new_keyword.replace(/\,+$/, '')
        if( new_keyword.length )
          seoObj.keywords.push new_keyword
          videoPageData.setSeo seoObj
          scope.$apply()
        obj.val('')

      element.on 'keyup', (e) ->
        if e.keyCode is 188
          setNewSeoObj( $(@) )

      element.on 'keypress', (e) ->
        if e.keyCode is 13
          setNewSeoObj( $(@) )
          e.preventDefault()

  ])