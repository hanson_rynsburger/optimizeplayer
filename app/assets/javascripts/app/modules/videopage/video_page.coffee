"use_strict"
angular.module("optimizePlayer")

  .controller("VideoPageCtrl", ["$scope", "$rootScope", "$http", "$compile", "Project", "videoPageData", "LAYOUTS", ($scope, $rootScope, $http, $compile, Project, videoPageData, LAYOUTS) ->
    $scope.widgets = []

    $scope.init = (project) ->
      $scope.setLoading true
      $scope.project = new Project(project)

      videoPageData.init(project).then((data) ->
        $scope.widgets = videoPageData.getAllWidgets()
        $scope.slug = videoPageData.getSlug()
        $scope.template = videoPageData.getTemplate()
        $scope.settings = videoPageData.getSettings()
       , (err) ->
        console.log "error", err
      )

    $scope.savePage = ->
      # $scope.setLoading true
      unless angular.equals({},$scope.slug)
        videoPageData.savePage().then((data)->
          # $scope.setLoading false
          console.log "Saved success!"
        , (error)->
          # $scope.setLoading false
          console.log "Doesn't save something wrong"
        )
        console.log "Saving page..."
      else
        alert "url is blank"

    $scope.goToPreview = ->
      $scope.widgets = videoPageData.getAllWidgets()
      window.open("/videopages/"+$scope.project.id+"/preview?"+$.param(videopage: videoPageData.videopage))

    $scope.changeLayout = (layout)->
      $scope.template = videoPageData.setTemplate(layout)

    $scope.saveSlug = ->
      unless angular.equals({},$scope.slug)
        videoPageData.saveSlug($scope.slug).then ((data) ->
          console.log "Slug successly saved!"
        ), (error) ->
          console.log "Doesn't save something wrong"
        console.log "Saving slug..."
        notempty = false
      else
        alert "url is blank"

    $scope.setLoading = (loading) ->
      $scope.isLoading = loading

    $scope.layoutDone = ->
      $scope.setLoading false
      $timeout (->
      ), 0

  ])

  #this Direcive replace elements in ng-repeat on their real view
  .directive("proxy", ['$parse', '$injector', '$compile', '$http', ($parse, $injector, $compile, $http) ->
    replace: true
    link: (scope, element, attrs) ->
      a = undefined
      directive = undefined
      name = undefined
      nameGetter = undefined
      value = undefined
      valueGetter = undefined
      nameGetter = $parse(attrs.proxy)
      name = nameGetter(scope)
      value = `undefined`
      if attrs.proxyValue
        valueGetter = $parse(attrs.proxyValue)
        value = valueGetter(scope)
      directive = $injector.get('wg' + name.component + "Directive")[0]
      attrs[name.component] = value  if value isnt `undefined`

      $http.get(directive.templateUrl)
        .then (result)->
          a = $compile(result.data)(scope)
          element.replaceWith a
          directive.compile(element, attrs, null)(scope, element, attrs)
  ])
