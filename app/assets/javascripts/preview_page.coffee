#= require jquery
#= require ./lib/vendor/lodash.min.js
#= require ./lib/vendor/jquery.gridster.js

preview =
  gr: null
  col_w: 58
  col_y: 45
  widgets: null
  init: ->
    @widgets = $ '.widget-wrapper'
    # @_generate_css()
    # @antiGridster()
    @gridsterInit()

    @gridsterUpdateHeight()

    # setTimeout ->
    #   FB.XFBML.parse()
    # , 100

  #I don't want to use gridster here but i had to, coz very hard to display elements on the page in the right place
  gridsterInit: ->
    @gr = $(".gridster").gridster(
      widget_margins: [10, 10]
      widget_base_dimensions: [35, 35]
      max_size_x: 12
      serialize_params: ($w, wgd) ->
        col: wgd.col
        row: wgd.row
        size_x: wgd.size_x
        size_y: wgd.size_y


      draggable:
        items: ".gs_www"
        stop: (event, ui) ->
          setLayoutBackgroundHeight()
    ).data "gridster"

  gridsterUpdateHeight: ->
    self = @

    #here will watch for dynamic content
    if ($('.fb-comments').length > 0)
      setInterval ->
        fbcontent = $('.fb-comments')
        gridsterElement = fbcontent.parents('li')
        new_height = Math.round(fbcontent.height()/self.col_y)
        if parseInt(gridsterElement.attr('data-sizey')) != new_height
          self.gr.resize_widget gridsterElement, 12, new_height
      , 1000


  #code blow i tried to display elements on the page without gridster
  #but without success
  #in order to try this version need to change li to div in preview_videopage.html.haml
  #and change css li should be not absolute
  # _generate_css: ->
  #   _css = ""
  #   col_w = @col_w
  #   col_y = @col_y
  #   already_created_w = []
  #   already_created_y = []

  #   @widgets.each ->
  #     element = $(@)
  #     width = parseInt element.attr 'data-sizex'
  #     height = parseInt element.attr 'data-sizey'

  #     index_w = _.findIndex(already_created_w, width)
  #     index_y = _.findIndex(already_created_y, height)


  #     if index_w == -1
  #       _css += '[data-sizex="'+ width + '"]{width: '+ col_w*width + 'px}'
  #       #insert to already_created
  #       already_created_w.push width

  #     if index_y == -1
  #       _css += '[data-sizey="'+ height + '"]{min-height: '+ col_y*height + 'px}'
  #       #insert to already_created
  #       already_created_y.push height

  #   style = $('<style></style>').text(_css)
  #   $("head link[rel='stylesheet']").append(style)


  # antiGridster: ->
  #   col = @col_w
  #   rows_width = []

  #   #put elements on the page
  #   @widgets.each ->
  #     element = $(@)
  #     width = parseInt element.attr 'data-sizex'
  #     offset = parseInt element.attr 'data-col'
  #     row = parseInt element.attr 'data-row'
  #     index = _.findIndex(rows_width, {num: row})

  #     if width < 12
  #       if index != -1
  #         offset = offset-rows_width[index].width

  #       $(@).css "margin-left" : (col*(offset-1)) + 'px'

  #     #add width for row
  #     if index == -1
  #       rows_width.push
  #         num:  row
  #         width: width + (offset-1)
  #     else
  #       rows_width[index].width += width + (offset - 1 )
$ ->
  preview.init()