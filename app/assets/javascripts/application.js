// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require foundation
//= require sugar
//= require ./lib/vendor/jquery.gridster.js
//= require ./lib/vendor/jquery.nicefileinput.min.js
//= require ./lib/vendor/jquery-ui.min
//= require ./lib/flowplayer-3.2.12.min.js
//= require ./lib/flowplayer.controls-3.2.10.min.js
//= require ./lib/flowplayer.ipad-3.2.12.min.js
//= require ./lib/vendor/jquery-migrate-1.2.1.min.js
//= require ./lib/vendor/jquery.tinyscrollbar.min
//= require ./lib/vendor/jquery.easing.min
//= require ./lib/vendor/jquery.dropkick
//= require ./lib/vendor/jquery.dateformat
//= require ./lib/vendor/jquery.prettycheckable
//= require ./lib/vendor/jquery.fileinput
//= require ./lib/vendor/bootstrap-tagmanager
//= require ./lib/vendor/colorpicker
//= require ./lib/vendor/dropzone
//= require ./lib/vendor/smartslider
//= require ./lib/vendor/jquery.ui.widget
//= require ./lib/vendor/jquery.fileupload
//= require ./lib/vendor/lodash.min.js
//= require ./lib/angular.min
//= require ./lib/angular-resource.min
//= require ./lib/angular-redactor
//= require ./lib/angular.tree
//= require ./app/optimizeplayer
//= require ./pages/split-test
//= require_tree ./app/controllers
//= require_tree ./app/services
//= require_tree ./app/directives
//= require_tree ./app/filters
//= require_tree ./app/modules
//= require main
//= require ./lib/angular-sanitize
//= require ./lib/ng-infinite-scroll.min
//= require ./preview_page.coffee
//= require redactor-rails
//= require redactor-rails/config

$(document).foundation();

$(function() {
  $('#add-service').foundation('reveal', 'open');
});
