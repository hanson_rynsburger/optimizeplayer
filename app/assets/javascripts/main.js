(function(jQuery) {
  // set the x-csrf-token for each ajax operation
  // rack / rack_csrf handle the rest
  jQuery.ajaxSetup({
    beforeSend: function(xhr) {
      var token = jQuery('meta[name="csrf-token"]').attr('content');
      xhr.setRequestHeader('X-CSRF-TOKEN', token);
    }
  });
}(jQuery));

// Key Localhost = $411293955881174
// Key Production = $619699884196610
var keyFlowplayer = '$411293955881174';
var modalCsnFiles = '';
var modalCsnMenu = '';
var folderTree = '';
var regexFile = /\.[0-9a-z]{3,4}$/i;
var regexFileImage = /\.(gif|jpg|jpeg|tiff|png)$/i;
var regexFileVideo = /\.(mp4|ogv|webm)$/i;
var regexFileAudio = /\.(mp3)$/i;
var fileDestination = '';

$(document).on("click", '#add_team_member', function(){
	$('#account .team .invite').slideDown(500);
	return false;
});

$(document).ready(function () {

	// Replace all links with ajax calls.
	$('a').delegate(function(ev){
		ev.preventDefault();
		alert('efil');
	});

	//Custom Checkbox and Radio Button
	$('input.pretty').prettyCheckable();

	 //Custom Input File
	 //$('.filezile').file();

	// Open User Menu
	//$('header .open-user-dropdown').on('click', function(){
	//	$('header .user-dropdown').stop().slideToggle(250);
	//	$(this).toggleClass('selected');
	//});

	$('.horizontal-scrollbar').tinyscrollbar({
  		axis: 'x',
  		sizethumb: '82'
	});

	// Select All Files
	$('#my-uploads .main-area .select-all input[type=checkbox]').on('change', function(){
		if($(this).is(':checked')){
			$('#my-uploads .files .col1 input[type=checkbox]').each(function(){
				this.setAttribute("checked", "checked");
				this.checked = true;
			});
		}else{
			$('#my-uploads .files .col1 input[type=checkbox]').each(function(){
				this.setAttribute("checked", ""); // For IE
				this.removeAttribute("checked"); // For other browsers
				this.checked = false;
			});
		}
	});
	$('#my-uploads .main-area .active-checkbox').on('click', function(){
		$('#my-uploads .main-area .select-all input[type=checkbox]').click();
		return false;
	});

	// Tooltip
	$('.use-tooltip').on('mouseenter', function(){
		var type = $(this).data('tooltip');
		var title = $(this).data('title');
		var tooltip = $('#tooltip-' + type);
		$(this).append(tooltip);
		$(tooltip).find('span').html(title).end().stop().fadeIn(250);
	}).on('mouseleave', function(){
		var tooltip = $(this).data('tooltip');
		$('#tooltip-' + tooltip).stop().fadeOut(250);
	});

	// Tooltip Individual
	$('#dashboard .favourites ul li a, #my-projects.inside .project .content > li, #dashboard  .project .content > li, #split-test .split li').on('mouseenter', function(){
		$(this).find('.tooltip').stop().fadeIn(250);
	}).on('mouseleave', function(){
		$(this).find('.tooltip').stop().fadeOut(250);
	});

	$('#skins .skin').on('click', function(){
		var skin = $(this).data('skin');
		$('#final-video').attr('class', skin);
		return false;
	});

	// Screen Size
	$('#final-video .show').on('click', function(){
		$('#final-video .sizes').slideDown(250);
		return false;
	});
	$('#final-video .hide').on('click', function(){
		$('#final-video .sizes').slideUp(250);
		return false;
	});
	$('#final-video .sizes .predefined a').on('click', function(){
		$(this).siblings('.selected').removeClass('selected').end().addClass('selected');
		$('#final-video .screen-size-final').val($(this).data('value'));
		return false;
	});
	$('#final-video .sizes .custom input[type=text]').on('blur', function(){
		$('#final-video .sizes .predefined a.selected').removeClass('selected');
		size = $('#final-video .sizes .custom .width').val();
		size += 'x';
		size += $('#final-video .sizes .custom .height').val();
		$('#final-video .screen-size-final').val(size);
	});

  	// Setting Control Bar
	$('#control-bar .checkboxes').on('click', function(){
		var control = $(this).find('input[type=checkbox]').data('control');
		$('#final-video .player').toggleClass(control);
	});


	// Setting Player Stretching
	$('#player-stretching .radios').on('click', function(){
		var control = $(this).find('input[type=radio]').data('control');
		alert(control);
	});

	// Setting Align Video
	$('#align-video .radios').on('click', function(){
		var control = $(this).find('input[type=radio]').data('control');
		alert(control);
	});

	// Dropzone
	$('#video-select .upload-area').dropzone({
		url: $('#video-select .upload-area').data('action'),
		clickable: false
	});
	$('#dropzone .upload-action').dropzone({
		url: $('#dropzone .upload-action').data('action')
	});
	$('#dashboard .upload-action').dropzone();

	// Dropzone in Footer
	$('footer .dropzone .upload-action').dropzone();

	//$('#my-uploads .main-area .upload-action').dropzone();


	//fiture out why isnt working
	//myUploadsDropzone.on("sending", function(file, xhr) { $.rails.CSRFProtection(xhr); });


	// Show/Hide Bottom Dropzone
	$('section[role=dropzone] .tab-dropzone').on('click', function(){
		var text = $(this).html();
		$('footer').toggleClass('no-padding');
		$(this).siblings('.dropzone').stop().slideToggle(500);
		if(text == 'HIDE'){
			text = 'SHOW';
		}else{
			text = 'HIDE';
		}
		$(this).html(text).toggleClass('caret-up');
	});

});

function extractTreeFolder(tree, counter){
	if(counter <= 0) {
		folderTree += '<ul>';
	}else{
		folderTree += '<ul style="display: none">';
	}

	for (var i = tree.length - 1; i >= 0; i--) {
		if(regexFile.test(tree[i].root)){
			addClass = 'file';
		}else{
			addClass = 'folder';
		}

		folderTree += '<li>';
		folderTree += '		<a href="#" class="' + addClass + '" data-path="' + tree[i].path + '">';
		folderTree += '			<i class="expand"></i><i class="icon folder-a"></i>';
		folderTree += '			<span>' + tree[i].root + '</span>';
		folderTree += '		</a>';
		if(tree[i].content.length >= 1){
			counter ++;
			extractTreeFolder(tree[i].content, counter);
		}
		folderTree += '</li>';
	}
	folderTree += '</ul>';

	return false;
}

function actionFolder(el, csnId){
	if(!$(el).hasClass('extracted')){
		$('#modal-csn .menu .main-csn a.folder').removeClass('selected');
		$(el).addClass('selected').addClass('extracted');
		modalCsnFiles.tinyscrollbar_update();
		modalCsnMenu.tinyscrollbar_update();

		$.get('/csns/'+ csnId +'/files', function(data){
			data = eval(data);
			extractTreeFolder(data, 0);
			$(el).after(folderTree);
			$(el).find('i.expand').toggleClass('minus');

			$(el).parent().find('a.folder').on('click', function(){
				var list = $(this).siblings('ul');
				var files = '';
				$('#modal-csn .menu .main-csn a.folder').removeClass('selected');
				$(this).addClass('selected').find('i.expand').toggleClass('minus');
				$(list).slideToggle(250, function(){
					modalCsnMenu.tinyscrollbar_update();
				});
				$(list).find('> li > .file').each(function(){
					var name = $(this).find('span').html();
					var path = $(this).data('path');
					files += '<li>';
					files += '	<a href="#" class="file" data-path="' + path + '">';
					files += '		<span class="icon-song">' + name + '</span>';
                    files += '	</a>';
                    files += '</li>';
				});
				$('#modal-csn .files .overview ul').html(files);

				modalCsnFiles.tinyscrollbar_update();

				$('#modal-csn .files .file').on('click', function(){
					$('#modal-csn .details')
						.find('.name').html('').end()
						.find('.kind').html('').end()
						.find('.size').html('').end()
						.find('.modified').html('').end()
						.find('input.file-url').val('');
					$('#modal-csn .details .object').html('');
					$('#modal-csn .details .loading').show();

					$('#modal-csn .files .file').removeClass('selected');
					$(this).addClass('selected');
					var name = $(this).data('path');
					$.post('/csns/'+ csnId +'/file.json', { filename : name }, function(data){
						$('#modal-csn .select-error').hide();
						$('#modal-csn .details .loading').hide();
						$('#modal-csn .details')
							.find('.name').html(shortenFileName(data.filename)).end()
							.find('.kind').html(data.extension.toUpperCase().replace('.','')).end()
							.find('.size').html(humanize_filesize(data.size)).end()
							.find('.modified').html($.format.date(data.modified, "dd/MM/yyyy")).end()
							.find('input.file-url').val(data.url);

						if(regexFileVideo.test(data.filename)){
							$('#modal-csn .details .object').html('<div id="file-embed"><video><source type="video/' + data.extension.replace('.','') + '" src="' + data.url + '"/></video></div>');
							$('#file-embed').flowplayer();
						}else if(regexFileAudio.test(data.filename)){

						}else if(regexFileImage.test(data.filename)){
							$('#modal-csn .details .object').html('<img class="file-image" src="' + data.url + '"/>');
						}

					});

					return false;
				});

				return false;
			});
		});
	}

	return false;
}

function selectFileFromFinder(){
	var fileurl = $('#modal-csn .details input.file-url').val();
	fileurl = fileurl.replace(/%2F/gi, '/');
	if(fileurl.length > 0) {
		extension = fileurl.split(".");
		extension = extension[extension.length-1];

		$('#modal-csn .details')
			.find('.name').html('').end()
			.find('.kind').html('').end()
			.find('.size').html('').end()
			.find('.modified').html('').end()
			.find('.object').html('').end()
			.find('input.file-url').val('');

		if(fileDestination == 'final-video'){
			var logourl = $('#dropzone input.file-location').val();
			if(logourl.length <= 0){
				logourl = '/assets/logo-sample.png';
			}
			$('#video-select').parents('.row').hide();
			$('#final-video').show().find('.player').html('<video><source type="video/' + extension + '" src="' + fileurl + '"/></video>');
	  		$('#final-video .player').flowplayer({
				key: keyFlowplayer,
				logo: logourl
			});
			$('#final-video .player .fp-logo').addClass('left-bottom');
		}else if(fileDestination == 'dropzone'){
			$('#dropzone .preview-logo')
				.find('.image').attr('src', fileurl).end()
				.find('.name').html(fileurl).end()
				.show();
			$('#final-video .player .fp-logo img').attr('src', fileurl);
		}

  		$('#'+ fileDestination +' input.file-location').val(fileurl);
  		$('#modal-csn a.close-reveal-modal').trigger('click');

	}else{
		$('#modal-csn .select-error').show();
	}

	return fileurl;
}

function shortenFileName(file){
	if(file.length >= 20){
		var begin = file.substr(0,7);
		var end = file.substr(-7);
		file = begin + '...' + end;
	}
	return file;
}

function round_number(num, dec) {
	return Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
}

function humanize_filesize(fs) {
	if (fs >= 1073741824) { return round_number(fs / 1073741824, 2) + ' GB'; }
	if (fs >= 1048576)    { return round_number(fs / 1048576, 2) + ' MB'; }
	if (fs >= 1024)       { return round_number(fs / 1024, 0) + ' KB'; }
	return fs + ' B';
};
