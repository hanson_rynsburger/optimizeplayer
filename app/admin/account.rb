ActiveAdmin.register Account, as: "Account" do
  index do
    column :name
    column :users do |acc|
      "#{acc.users.count}/#{acc.plan.try(:number_of_users)}"
    end
    column :projects do |acc|
      acc.projects.count
    end
    column :plan do |acc|
      acc.plan.try(:name)
    end
    default_actions
  end

  show do |acc|
    attributes_table do
      row :name
      row :users do
        acc.users.count
      end
      row :number_of_jobs
      row :customer_id
      row :logged_sites 
    end
  end

end
