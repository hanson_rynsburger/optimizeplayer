ActiveAdmin.register AddonsPricelist, as: "Addons Prices" do
  config.filters = false
  config.batch_actions = false

  actions :index, :edit, :update

  index do
    column "Screenshare support", :formatted_screenshare_support
    column "Custom skin", :formatted_custom_skin
    column "Video importing", :formatted_video_importing
    column "Pay as you go", :formatted_pay_as_you_go
    default_actions
  end

  form do |f|
    f.inputs do
      f.input :formatted_screenshare_support, label: "Screenshare support"
      f.input :formatted_custom_skin, label: "Custom skin"
      f.input :formatted_video_importing, label: "Video importing"
      f.input :formatted_pay_as_you_go, label: "Pay as you go"
    end

    f.actions
  end
end
