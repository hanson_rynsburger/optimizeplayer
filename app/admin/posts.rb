ActiveAdmin.register Post, :as => 'News' do

  index do
    column :id
    column :title
    column :created_at
    column :updated_at
    default_actions
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :body, input_html: { class: 'redactor', :rows => 40, :cols => 120 }
    end
    f.actions
  end

end
