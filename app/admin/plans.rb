ActiveAdmin.register Plan do

  index do
    column :name
    column :remote_id
    column :number_of_embeds
    column :number_of_users
    column :number_of_jobs
    column :has_branding
    column :has_ads
    column :interval
    column :can_add_cta
    column :amount
    column :file_size_limit
    default_actions
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :remote_id
      f.input :number_of_embeds
      f.input :number_of_users
      f.input :number_of_jobs
      f.input :has_branding
      f.input :has_ads
      f.input :can_add_cta
      f.input :interval, as: :select, collection: ["month", "week", "year"]
      f.input :trial_period_days
      f.input :file_size_limit
      f.input :amount
    end
    f.actions
  end
end
