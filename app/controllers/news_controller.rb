class NewsController < ApplicationController

  def index
    @posts = Post.order("created_at DESC").page(params[:page]).per(5)
    current_user.last_readed_post = Time.now
    current_user.save
    respond_to do |format|
      format.html
      format.json { render json: @posts }
    end
  end

  def show
     @posts = Post.where('id = ?',params[:id])
    respond_to do |format|
      format.html
      format.json { render json: @posts }
    end
  end

  def count
    count = {}
    last_readed_at = current_user.last_readed_post || 2.month.ago
    count[:count] = Post.where('created_at >= ?', last_readed_at).count
    respond_to do |format|
      format.json { render json: count }
    end
  end

end
