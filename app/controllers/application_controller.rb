class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :authenticate_user!

  before_filter :check_subdomain

  layout Proc.new { |controller| controller.request.xhr? ? false : 'application' }

  helper_method :current_account

  def authenticate_admin_user!
    redirect_to new_user_session_path unless current_user.try(:is_admin?)
  end

  def after_sign_in_path_for(user)
    unless user.url.blank?
      dashboard_url(:subdomain => user.url)
    else
      # dashboard_url(:subdomain => "admin")
      root_path
    end

  end

  def after_sign_out_path_for(resource_or_scope)
    dashboard_url(:subdomain => false)
  end

  def current_account
    current_user.account
  end

  def check_subdomain
    current_subdomain = request.subdomain(2)
    if current_user and !current_user.url.blank?
      if current_subdomain != current_user.url
        redirect_to dashboard_url(:subdomain => current_user.url)
      end
    else
      unless current_subdomain.blank?
        redirect_to dashboard_url(:subdomain => false)
      end
    end
  end

end
