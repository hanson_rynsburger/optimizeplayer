class Account::BillingController < AccountController

  respond_to :html

  def index
    @plans = Plan.order("amount ASC")
    @current_plan = current_account.subscription.try :plan
  end

end
