class GettingStartedController < ApplicationController

  def index
    @tasks = current_user.account.getting_starteds.order('updated_at DESC')
    render json: @tasks
  end

  def destroy
   render nothing: true, status: 200 if current_user.account.getting_starteds.destroy(params[:id])
  end
end
