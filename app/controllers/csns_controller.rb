class CsnsController < ApplicationController

  respond_to :html
  before_filter :authenticate_user!

  def index
  end

  def directories
    csn = current_account.csns.find(params[:id])

    if params[:bucket]
      render json: csn.directory_tree(params[:bucket])
    else
      render json: csn.directories
    end
  end
end
