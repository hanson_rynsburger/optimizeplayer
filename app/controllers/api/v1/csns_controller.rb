module Api
  module V1
    class CsnsController < ApplicationController
      inherit_resources
      respond_to :json

      def create
        @csn = current_user.account.csns.new(params[:csn])
        begin
          if @csn.save
            respond_to do |format|
              format.json { render json: @csn }
            end
          end

        rescue Excon::Errors::Unauthorized
          error ||= "Invalid credentials"
        rescue Excon::Errors::Forbidden
          error ||= "An error occurred while trying to create this bucket. It look like this bucket already exists but does so under a different account which you do not have access to."
        rescue Excon::Errors::Conflict => e
          error ||= "An error occurred while trying to create this bucket. This bucket probably already exists under your account but in a different region."
        ensure
          if @csn.new_record?
            if !@csn.errors.empty?
              render text: @csn.errors.full_messages.join("<br>"), status: :error
            else
              render text: error, status: :error
            end
          end
        end
      end

      protected

      def begin_of_association_chain
        current_user.account
      end
    end
  end
end
