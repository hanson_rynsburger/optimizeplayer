module Api
  module V1
    class CtasController < ApplicationController
      respond_to :html, :js, :json
      respond_to :csv, only: [:export]

      before_filter :find_project

      def index
        render json: @project.ctas
      end

      def create
        attrs = params[:cta].except(:cid)
        case attrs[:type]
          when 'CtaButton'
            @project.cta_buttons.create(attrs)
          when 'CtaHtml'
            @project.cta_htmls.create!(attrs)
          when 'CtaOptin'
            @project.optin = CtaOptin.create(attrs)
        end
        @project.save!
        render nothing: true, status: :ok
      end

      def update
        cta = Cta.find_by_id(params[:id])
        if cta.update_attributes(params[:cta].except(:cid, :id))
          cta.project.save!
          render json: cta.as_json
        end
      end

      def destroy
        cta = Cta.find_by_id(params[:id])
        if cta.destroy
          cta.project.save!
          render nothing: true, status: :ok
        end
      end

      private
      def find_project
        @project = Project.find_by_cid(params[:project_id])
      end
    end
  end
end