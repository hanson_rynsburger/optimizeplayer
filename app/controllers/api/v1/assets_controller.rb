module Api
  module V1
    class AssetsController < ApplicationController
      inherit_resources
      include UploadsHelper
      before_filter :authenticate_user!

      respond_to :json

      def rackspace_streaming_url
        filename = params[:filename]
        csn_id   = params[:csn_id]
        storage  = current_user.account.csns.find(csn_id)
        result = {url: "#{storage.rs_streaming_url}/#{filename}"}
        respond_with result
      end

      def upload_form_params
        filename = params[:filename]
        csn_id   = params[:csn_id]
        storage  = current_user.account.csns.find(csn_id)

        respond_with generate_upload_form_params(storage, filename)
      end

      def copy_to_cloudfront
        @asset = current_user.assets.find(params[:id])
        @asset.copy_to_cloudfront

        respond_with({ remote_url: @asset.remote_url })
      end

      def show
        @asset = Asset.find(params[:id])
        respond_with(@asset)
      end

      def download_progress
        @asset = current_user.assets.find(params[:id])
        heywatch_download = HEYWATCH.info(:download, @asset.download_job_id)

        if heywatch_download["status"] == "finished"
          @asset.encode(heywatch_download["video_id"])
        end

        render json: {progress: heywatch_download["progress"]}
      end

      def encode_progress
        @asset = current_user.assets.find(params[:id])
        render json: {progress: {percent: HEYWATCH.info(:job, @asset.encoding_job_id)["progress"]}}
      end

      def create
        needs_encoding = params[:asset].delete(:needs_encoding)

        @asset = current_user.assets.new(params[:asset])
        @asset.attachment = params[:attachment]

        if @asset.save
          if needs_encoding && (@asset.attachment.present? || @asset.remote_url.present?)
            authorize! :encode, @asset
            @asset.prepare_attachment
          end
          respond_to do |format|
            format.json { render json: @asset }
          end
        end
      end

      def destroy
        @asset = current_user.assets.find(params[:id])
        @asset.destroy
        respond_to do |format|
          format.html { redirect_to my_uploads_path }
          format.json { head :no_content }
        end
      end
    end
  end
end
