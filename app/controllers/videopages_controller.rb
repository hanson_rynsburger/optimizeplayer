class VideopagesController < ApplicationController
  respond_to :html, :js, :json
  before_filter :find_project, :only => [:edit]
  skip_filter :authenticate_user!, only: [:show]

  skip_before_filter :check_subdomain, only: [:show]

  def edit
    if !@project.blank? and @project.videopage.blank?
      @project.build_videopage
      @project.save
    end
  end

  def preview
    unless params[:videopage].blank?
      @preview_page = params

      unless params[:videopage][:project_id].blank?
        @project_preview = current_account.projects.find_by_id(params[:videopage][:project_id])
      end
    end
    render :action => "preview", :layout => "video_preview"
  end

  def show
    unless current_subdomain.blank? and params[:user_url].blank?
      user_with_current_subdomain = User.find_by_url(current_subdomain)
      # videopage_with_current_url = Videopage.find_by_slug(params[:user_url])
      unless user_with_current_subdomain.blank?
        videopage_with_current_url = Videopage.where(:user_id=>user_with_current_subdomain.id).where(:slug=>params[:user_url]).first
      end

      if !videopage_with_current_url.blank? and !user_with_current_subdomain.blank? and user_with_current_subdomain.id == videopage_with_current_url.user_id
        preview_data = { :videopage => videopage_with_current_url }

        parse_params = ['widgets', 'seo']
        parse_params.each do |parse_param|
          preview_data[:videopage][parse_param] = parse_json(preview_data[:videopage][parse_param])
          p preview_data[:videopage][parse_param]
        end

        @preview_page = preview_data
        @project_preview = Project.find_by_id(videopage_with_current_url[:project_id])
        render :action => "show", :layout => "video_preview"
      else
        respond_to do |format|
          format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
          format.xml  { head :not_found }
          format.any  { head :not_found }
        end
      end
    else
      render :action => "show", :layout => "video_preview"
    end
  end

  private
  def find_project
    @project = current_account.projects.find_by_cid(params[:id])
  end

  def parse_json(data)
    unless data.blank?
      ActiveSupport::JSON.decode(data)
    end
  end
end