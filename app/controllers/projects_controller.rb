require 'csv'

class ProjectsController < ApplicationController
  load_and_authorize_resource find_by: :cid, except: [:show, :new, :create]

  respond_to :html, :js, :json
  respond_to :csv, only: [:export]

  before_filter :find_project, only: [:edit, :destroy, :update, :set_position]

  skip_filter :authenticate_user!, only: [:show]

  def index
    @search = params[:search]
    respond_to do |format|
      format.html
      format.json { render json: current_account.projects }
    end
  end

  def export
    csv = CSV.generate do |csv|
      csv << ['Project title', 'Embed code']
      ids = params[:ids]
      ids = ids.values unless ids.is_a?(Array) # if ids is a hash
      Project.where(id: ids).each do |project|
        csv << [project.title, "<script src='#{request.base_url}/projects/#{project.cid}'></script>"]
      end
    end

    respond_to do |format|
      format.csv { send_data csv, type: Mime::CSV, disposition: "attachment; filename=projects-#{Date.today.to_s}.csv" }
    end
  end

  def new
    @csns = current_account.csns
  end

  def show
    @project = Project.find_by_cid(params[:id])

    if !@project.allowed_urls.present? || @project.allowed_urls.index(URI(request.referer).host)
      redirect_to @project.cdn_url(request.referer)
    else
      render nothing: true, status: 403
    end
  end

  def edit
  end

  def create
    project = current_account.projects.create(params[:project])

    if project.save
      render json: project, status: :ok
    else
      render nothing: true, status: :error
    end
  end

  def update
    if @project.update_attributes(params[:project])
      render json: @project, status: :ok
    else
      render nothing: true, status: :error
    end
  end

  def destroy
    @project.destroy
    redirect_to :back
  end

  def set_position
    if params[:position]
      @project.set_list_position(params[:position].to_i)
    end

    respond_with @project
  end

  private
  def find_project
    @project = current_account.projects.find_by_cid(params[:id])
  end
end
