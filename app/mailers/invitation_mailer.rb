# -*- coding: utf-8 -*-
class InvitationMailer < ActionMailer::Base
  default from: "OptimizePlayer <robot@optimizeplayer.com>"

  def invitation_message(user, password)
    @user = user
    @pass = password
    @account_name = @user.account.name
    mail(:to => @user.email,
       :subject => "#{@account_name} Has Invited You to Join Their OptimizePlayer Team")
  end
end
