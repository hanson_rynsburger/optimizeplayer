class Account < ActiveRecord::Base
  has_many :users
  has_many :csns
  has_many :projects
  has_many :folders
  has_many :transactions
  has_and_belongs_to_many :getting_starteds

  has_one :subscription

  attr_accessible :name

  after_create do
    self.folders.create(name: 'uncategorized')
  end

  def plan
    subscription.try(:plan)
  end
  
  def create_subscription(customer_token, plan)
    unless subscription.present?
      plan = Plan.find_by_remote_id(plan)
      self.subscription = Subscription.create!(plan: plan,
                                               account: self,
                                               status: "trialing",
                                               start: Date.today)
      self.update_attribute(:customer_id, customer_token)
    end
  end

  def create_customer(plan)
    customer = Stripe::Customer.create(
      :plan => plan,
      :email => self.users.find_by_owner(true).email
    )
    self.create_subscription(customer.id, plan)
  end

  def customer
    Stripe::Customer.retrieve(self.customer_id)
  end

  def has_credit_card?
    self.last4.present?
  end

  def plan?(plan_name)
    self.subscription.plan.name.to_sym.eql?(plan_name)
  end
end
