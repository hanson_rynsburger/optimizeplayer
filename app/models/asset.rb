class Asset < ActiveRecord::Base
  attr_accessible :attachment, :original_filename, :user_id, :user, :project_id, 
                  :file_origin, :url, :format, :media_type, :keep_video_size, :remote_url,
                  :encoding_job_id, :needs_encoding, :csn_id, :key
                    
  belongs_to :user
  belongs_to :project
  belongs_to :csn

  def prepare_attachment
    if csn.provider == 'rackspace'
      url = "#{csn.rs_public_url}/op-#{self.original_filename}"
    else # aws
      url = self.remote_url
    end
    download_job = HEYWATCH.create(:download, url: url, title: "#{user.account.id}_#{id}_#{self.created_at}")
    self.update_attribute(:download_job_id, download_job["id"])
  end

  def encode(video_id)
    update_attribute(:video_id, video_id)
    if csn.provider == 'rackspace'
      directive = cf_directive
      url = cf_url
    else # aws
      directive = s3_directive
      url = s3_url
    end

    encoding_job = HEYWATCH.create(:job, video_id: video_id,
                          format_id: 42361,
                          keep_video_size: self.keep_video_size,
                          output_url: directive)

    update_attributes(encoding_job_id: encoding_job["id"], remote_url: url)
    user.account.increment!(:number_of_jobs)
  end

  def s3_directive
    "s3://#{csn.access_key}:#{csn.secret_key}@#{csn.storage_service.bucket_key}/uploads/#{self.original_filename.split('.')[0].gsub(/[\[\]\{\}]/, "_")}-ps.mp4"
  end

  def cf_directive
    "cf://#{csn.access_key}:#{csn.secret_key}@#{csn.storage_service.bucket_key}/op-#{self.original_filename.split('.')[0].gsub(/[\[\]\{\}]/, "_")}-ps.mp4"
  end

  def s3_url
    "https://s3.amazonaws.com/#{csn.storage_service.bucket_key}/uploads/#{self.original_filename.split('.')[0]}-ps.mp4"
  end

  def cf_url
    "#{csn.rs_streaming_url}/op-#{self.original_filename.split('.')[0].gsub(/[\[\]\{\}]/, "_")}-ps.mp4"
  end

  def url
    if attachment.present?
      attachment.to_s
    else
      remote_url
    end
  end

  def copy_to_cloudfront
    bucket, path = *key.split("/", 2)
    csn.storage_service.copy_file_to_csn_bucket(bucket, path)
    update_attribute(:remote_url, "https://#{csn.cloudfront}/uploads/#{path}")
  end

  def as_json(options={})
    super options.merge(
      only:     [:id, :file_origin, :download_job_id, :remote_url, :attachment, :original_filename, :content_type, :keep_video_size, :csn_id, :needs_encoding],
      methods:  [:url]
    )
  end

end
