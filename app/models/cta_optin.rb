class CtaOptin < Cta
  %w[ button_background_color button_background_opacity header_text skip preview].each do |key|
    attr_accessible key
    scope "has_#{key}", lambda { |value| where("options @> (? => ?)", key, value) }

    define_method(key) do
      options && options[key]
    end

    define_method("#{key}=") do |value|
      self.options = (options || {}).merge(key => value)
    end
  end
end
