class GettingStarted < ActiveRecord::Base
  attr_accessible :short_description, :title, :url
  has_and_belongs_to_many :accounts

  after_create :add_tasks_to_accounts
  after_destroy :remove_tasks_from_accounts



  validates :url, format: { with: URI::regexp(%w(http https)) }
  private

  def remove_tasks_from_accounts
   AccountsGuideTask.destroy_all(guide_task_id: self.id)
  end

  def add_tasks_to_accounts
    accounts = Account.all
    accounts.each do |account|
      account.getting_starteds.push(self)
    end
  end
end
