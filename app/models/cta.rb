class Cta < ActiveRecord::Base
  serialize :options, ActiveRecord::Coders::Hstore

  attr_accessible :html, :options, :project_id, :type
  belongs_to :project

  def as_json(options={})
    {
      cuepoints: cuepoints ? cuepoints[1..-2].split(",").map(&:to_i) : [],
      id: id,
      cid: id,
      type: type,
      html: html
    }.merge(self.options.except("cuepoints"))
  end

  %w[ background_color font_family font_style font_size text_color location
      cuepoints on_start on_pause on_finish on_cuepoints position text link
      background_opacity text_opacity left top right bottom fullscreen width height].each do |key|
    attr_accessible key
    scope "has_#{key}", lambda { |value| where("options @> '#{key}=>#{value}'") }

    define_method(key) do
      options && options[key]
    end

    define_method("#{key}=") do |value|
      self.options = (options || {}).merge(key => value)
    end
  end
end
