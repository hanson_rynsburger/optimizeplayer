class Subscription < ActiveRecord::Base
  attr_accessible  :plan, :plan_id, :account, :customer_token, :start, :status

  belongs_to :account
  belongs_to :plan

  validates :account_id, :plan_id, presence: true

  def trialing?
    status == "trialing" && !plan.is_free
  end

  def unpaid?
    status == "unpaid"
  end

  def cancel_subscription
    account.customer.cancel_subscription
  end

  def retrieve_subscription
    return account.customer.subscription
  end

  def update_subscription(plan_id)
    plan = Plan.find_by_remote_id(plan_id)
    if account.customer.update_subscription(plan: plan_id)
      update_attribute(:plan, plan)
    end
  end

  def downgradable?
    account.projects.count < plan.number_of_embeds && account.users.count <= plan.number_of_users
  end
end
