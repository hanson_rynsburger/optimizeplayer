class User < ActiveRecord::Base
  belongs_to :account
  has_many :assets
  has_and_belongs_to_many :folders
  devise :database_authenticatable, :registerable, :recoverable, :rememberable #, :trackable, :validatable, :confirmable, :lockable

  attr_accessible :name, :email, :password, :password_confirmation, :remember_me, :admin, :folder_ids, :url

  validates :email, uniqueness: true
  validates :url, uniqueness: true

  alias_method :db_folders, :folders

  after_create do |user|
    unless user.account
      account = Account.create(name: "#{user.name}account")
      user.account_id = account.id
      user.owner = true
      user.save!
      user.account.getting_started_ids = GettingStarted.select(:id).map{|t| t.id}
    end
  end

  def folders
    owner ? account.folders : db_folders
  end

  def username
    name.present? ? name : email
  end

  def embeds_count
    account.projects.count
  end

  def has_account?
    self.account.present?
  end

  def has_csn?
    self.account.present? && self.account.csns.any?
  end

  def is_admin?
    self.admin
  end

  def self.create_team_member(account, email)
    pass = SecureRandom.base64(6)
    member = account.users.create(email: email, password: pass, password_confirmation: pass)
    InvitationMailer.invitation_message(member, pass).deliver
  end
end
