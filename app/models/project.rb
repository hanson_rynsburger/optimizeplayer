class Project < ActiveRecord::Base
  serialize :flowplayer, ActiveRecord::Coders::Hstore

  belongs_to :account
  belongs_to :folder
  has_many :assets
  has_one  :optin, class_name: "CtaOptin", foreign_key: "project_id"
  has_one  :videopage
  has_many :cta_buttons, class_name: "CtaButton", foreign_key: "project_id"
  has_many :cta_htmls, class_name: "CtaHtml", foreign_key: "project_id"

  acts_as_list scope: :folder

  validates :cid, uniqueness: true, presence: true

  attr_accessible :allowed_urls, :analytics, :flowplayer, :tags, :title, :license_key, :folder_id, :favorite, :responder_code, :asset_ids

  after_update :upload_embed

  before_validation do
    self.cid ||= SecureRandom.hex(16)
  end

  after_create do
    unless self.folder.present?
      self.folder = self.account.folders.first
    end
    self.save
    self.insert_at(folder.projects.where{(favorite == true) | (dashboard == true)}.count + 1)
  end

  def folder_id=(folder_id)
    if self[:folder_id] != folder_id
      self.remove_from_list
      self[:folder_id] = folder_id
    end    
  end

  def date
    I18n.l(created_at, format: '%d %b %Y')
  end

  # TODO because of "multiple source of truth" problem in js
  # i decided to write this monley patch. It should be rewritten
  # as regular field in the future.
  def filename
    if url
      url.match(%r{([^\/]+\z)})[0]
    else
      nil
    end
  end

  def ctas
    {
      buttons: cta_buttons.order("id ASC").as_json,
      htmls: cta_htmls.order("id ASC").as_json,
      optin: optin.as_json
    }
  end

  def as_json(options={})
    super( 
      options.merge(
        only:     [:id, :cid, :title, :folder_id, :tags, :allowed_urls, :position, :favorite, :responder_code, :dashboard, :analytics],
        methods:  [:date, :filename],
        include:  [:assets, :cta_htmls]
      )
    ).merge(self.flowplayer).merge({
      cta_buttons: self.cta_buttons.as_json, 
      cta_htmls: self.cta_htmls.as_json,
      optin: self.optin.as_json})
  end

  def to_param
    cid
  end

  def paste_in_right_place
    # in case of more stability
    Rails.logger.warn "### Project Moving >> paste in right place"
    #Project.transaction do
      reload
      folder.reload

      ts = folder.two_stars_projects_count
      od = folder.only_dashboard_projects_count
      of = folder.only_favorite_projects_count
    
      right_position = if favorite and dashboard
                         1
                       elsif !favorite and dashboard
                         ts+1
                       elsif favorite and !dashboard
                         ts+od+1
                       else
                         ts+od+of+1
                       end 

      insert_at right_position
    #end
    self
  end

  def toggle_favorite
    toggle :favorite
    save!
    paste_in_right_place
    self
  end

  def toggle_dashboard
    if dashboard
      self.dashboard = false
    else
      self.dashboard = true
    end
    
    save!
    paste_in_right_place
    self
  end

  def upload_embed
    embed_storage = EmbedStorage.new(self)
    embed_storage.upload
  end

  def cdn_url(referer)
    ref_uri = URI(referer)
    host = ref_uri.host.split(".").last(2).join(".")
    key = FlowplayerLicense.new(host).key
    url = "#{Yetting.cloudfront_host}/embeds/#{account_id}/#{cid}#{updated_at.to_i}#{key}.js"

    uri = URI("http:#{url}")
    request = Net::HTTP.new uri.host
    response = request.request_head uri.path

    if response.code.to_i == 200
      url
    else
      EmbedStorage.new(self, key).upload
      account.update_attribute(:logged_sites, account.logged_sites.split(",").push(host).join(",")) unless account.logged_sites.index(host)
      url
    end
  end

  %w[ skin display_time display_volume play_button allow_embed embed_code full_screen 
      facebook_share twitter_share show_banner affiliate_link display_time_percentage display_buffer auto_mute
      url extension dimensions max_width logo logo_pos_left logo_pos_right mute scaling start_image

      optin_on_start optin_on_finish optin_on_pause optin_on_cuepoints optin_location optin_bg
      optin_text_color optin_font_family optin_text optin_button_text optin_button_bg optin_font_size
      cuepoints optin_text_opacity optin_button_bg_opacity optin_bg_opacity has_optin has_buttons has_htmls

      allow_pause loop auto_start sliderColor big_play_button control_bar_hide_method cb_gap

      skinColor align
    ].each do |key|
    attr_accessible key
    scope "has_#{key}", lambda { |value| where("flowplayer @> (? => ?)", key, value) }

    define_method(key) do
      flowplayer && flowplayer[key]
    end

    define_method("#{key}=") do |value|
      self.flowplayer = (flowplayer || {}).merge(key => value)
    end
  end

  def short_cid
    self.cid.first(4)
  end
end
