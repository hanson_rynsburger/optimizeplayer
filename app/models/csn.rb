class Csn < ActiveRecord::Base
  belongs_to :account
  has_many   :assets

  attr_accessible :access_key, :secret_key, :provider, :account_id, :active, :name, :bucket, :initialized
  # rackspace specific:
  attr_accessible :rs_temp_url_key, :rs_endpoint_url, :rs_endpoint_path, :rs_streaming_url, :rs_public_url

  # s3 specific:
  attr_accessible :use_cloudfront

  attr_accessor :storage_service

  PROVIDER_NAMES = { aws: 'Amazon S3', rackspace: 'Rackspace' }

  validates :account_id, :access_key, :secret_key, :name, :bucket, presence: true
  validates :provider, presence: true, inclusion: { in: ['aws', 'rackspace'] }
  validates :access_key, :secret_key, uniqueness: true
  validate :storage_service_credentials_are_valid

  after_create do 
    if use_cloudfront && provider == 'aws'
      storage_service.create_distribution
    end
  end

  def provider_name
    PROVIDER_NAMES[self.provider.to_sym]
  end

  def directories
    storage_service.get_buckets_list
  end

  def directory_tree(name)
    storage_service.get_bucket_tree(name)
  end

  def storage_service
    result =  case provider
              when 'aws'
                AwsStorage.new(self) if provider == 'aws'
              when 'rackspace'
                RackspaceStorage.new(self) if provider == 'rackspace'
              end
    unless initialized
      self.initialized = true

      result.apply_cors_rules
      if provider == 'rackspace'
        self.rs_temp_url_key  = result.connection.head_containers.headers['X-Account-Meta-Temp-Url-Key']
        self.rs_streaming_url = result.bucket.streaming_url
        self.rs_public_url    = result.bucket.public_url
        endpoint_uri          = result.connection.endpoint_uri
        self.rs_endpoint_url  = endpoint_uri.to_s
        self.rs_endpoint_path = endpoint_uri.path
      end
      self.save
    end
    result
  end

  def as_json(options = {})
    {
      id: id,
      provider_name: provider_name,
      provider: provider,
      name: name,
      access_key: access_key,
      secret_key: secret_key,
      bucket: bucket,
      use_cloudfront: use_cloudfront
    }
  end

  def storage_service_credentials_are_valid
    begin
      self.storage_service
    rescue Excon::Errors::Forbidden
      errors.add(:base, "Credentials are invalid")
    end
  end
end
