class Videopage < ActiveRecord::Base
	belongs_to :project
	belongs_to :user
	validate :valid_url, :on => :save
	serialize :url, ActiveRecord::Coders::Hstore
	serialize :template, ActiveRecord::Coders::Hstore
	attr_accessible :widgets, :project_id, :template, :url, :seo, :is_iframe, :slug, :user_id
	# serialize :widgets, JSON
	# serialize :url, JSON
	# serialize :template, JSON

	def valid_url
		self.errors.add(:url, "url is blank")
		if url.blank?
    	raise "url is blank"
    end
	end

end
