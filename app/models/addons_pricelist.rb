class AddonsPricelist < ActiveRecord::Base

  ACCOUNT_ATTRS_MAPPING = {
    pay_as_you_go: :prepaid,
    custom_skin: :number_of_skins,
    video_importing: :number_of_video_imports,
    screenshare_support: :screenshare_support
  }

  attr_accessible :custom_skin, :pay_as_you_go, :screenshare_support, :video_importing, 
                                :formatted_custom_skin, :formatted_pay_as_you_go, 
                                :formatted_screenshare_support, :formatted_video_importing

  validates_inclusion_of :is_singleton, :in => [true]

  acts_pricefully :custom_skin
  acts_pricefully :pay_as_you_go
  acts_pricefully :screenshare_support
  acts_pricefully :video_importing

  def self.instance
    first.present? ? first : AddonsPricelist.create!
  end

  def self.purchase(account, addon, quantity)
    quantity ||= 1
    customer = account.customer

    addon_price = self.instance.send(addon)*quantity.to_i
    description = "#{I18n.t(addon)} x#{quantity}"
    
    AddonsPricelist.transaction do
      charge = Stripe::Charge.create(
        amount: addon_price,
        currency: "usd",
        customer: customer.id,
        description: description
      )

      account_attr = self::ACCOUNT_ATTRS_MAPPING[addon.to_sym]

      if Account.columns_hash[account_attr.to_s].type == :boolean
        account.subscription.cancel_subscription if account_attr == :prepaid
        account.update_attribute(account_attr, true)
      else
        account.increment!(account_attr, quantity.to_i)
      end

      account.transactions.create(
                                  charge_id: charge.id,
                                  description: description, 
                                  amount: addon_price)

    end

  end
end
