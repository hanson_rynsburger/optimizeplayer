module BillingHelper
	def account_usage_bar(small=false)
    user_embeds = current_user.embeds_count 
    plan_embeds_count = current_account.subscription.plan.embeds_count
    
    percentage, plan_embeds = if current_account.prepaid || plan_embeds_count == -1
                  [0, "&#8734;"]
                 else
                  [(user_embeds * 100 / plan_embeds_count).to_i, plan_embeds_count]
                 end
    if small
      raw("<div class='progress large-6 alert round'>
          <span class='meter' style='width: #{percentage}%'></span>
          <span class='percent to-left' style='left: #{percentage}%'>#{user_embeds} / #{plan_embeds}</span>
        </div>")
    else
      raw("<div>
        <p>Account Usage</p>
        <div class='progress large-6 alert round'>
          <span class='meter' style='width: #{percentage}%'></span>
        </div>
        <span class='percent'>#{user_embeds} / #{plan_embeds}</span>
      </div>")
    end
  end
end
