# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140123105724) do

  create_table "accounts", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.string   "last4"
    t.boolean  "prepaid",                 :default => false
    t.integer  "number_of_skins",         :default => 0
    t.integer  "number_of_video_imports", :default => 0
    t.integer  "screenshare_support",     :default => 0
    t.string   "customer_id"
    t.integer  "number_of_jobs",          :default => 0
    t.text     "logged_sites",            :default => ""
  end

  create_table "accounts_getting_starteds", :force => true do |t|
    t.integer "account_id"
    t.integer "getting_started_id"
  end

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "addons_pricelists", :force => true do |t|
    t.integer  "custom_skin",         :default => 0
    t.integer  "video_importing",     :default => 0
    t.integer  "screenshare_support", :default => 0
    t.integer  "pay_as_you_go",       :default => 0
    t.boolean  "is_singleton",        :default => true
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
  end

  add_index "addons_pricelists", ["is_singleton"], :name => "index_addons_pricelists_on_is_singleton", :unique => true

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "assets", :force => true do |t|
    t.integer  "user_id"
    t.string   "attachment"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.string   "original_filename"
    t.string   "content_type"
    t.string   "key"
    t.integer  "csn_id"
    t.integer  "project_id"
    t.string   "url"
    t.string   "format"
    t.boolean  "active"
    t.string   "media_type"
    t.string   "file_origin"
    t.integer  "encoder_id"
    t.integer  "encoding_job_id"
    t.integer  "video_id"
    t.integer  "download_job_id"
    t.boolean  "keep_video_size",   :default => false
    t.string   "remote_url"
    t.string   "thumbnail"
  end

  create_table "csns", :force => true do |t|
    t.integer  "account_id"
    t.string   "provider"
    t.string   "access_key"
    t.string   "secret_key"
    t.string   "bucket"
    t.boolean  "active"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.string   "name"
    t.string   "cloudfront"
    t.boolean  "initialized",      :default => false
    t.string   "rs_temp_url_key"
    t.string   "rs_endpoint_url"
    t.string   "rs_endpoint_path"
    t.string   "rs_streaming_url"
    t.string   "rs_public_url"
    t.boolean  "use_cloudfront",   :default => false
  end

  add_index "csns", ["account_id"], :name => "index_cdns_on_company_id"

  create_table "cta", :force => true do |t|
    t.text     "html"
    t.integer  "project_id"
    t.hstore   "options"
    t.string   "type"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "encoders", :force => true do |t|
    t.integer  "account_id"
    t.string   "provider"
    t.string   "access_key"
    t.string   "secret_key"
    t.boolean  "active"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "name"
  end

  add_index "encoders", ["account_id"], :name => "index_encoders_on_company_id"

  create_table "folders", :force => true do |t|
    t.string   "name"
    t.integer  "account_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "parent_id"
  end

  create_table "folders_users", :force => true do |t|
    t.integer "folder_id"
    t.integer "user_id"
  end

  add_index "folders_users", ["folder_id"], :name => "index_folders_users_on_folder_id"
  add_index "folders_users", ["user_id"], :name => "index_folders_users_on_user_id"

  create_table "getting_starteds", :force => true do |t|
    t.string   "title"
    t.text     "short_description"
    t.string   "url"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "plan_lists", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "plans", :force => true do |t|
    t.string   "interval"
    t.string   "name"
    t.integer  "amount"
    t.string   "remote_id"
    t.string   "currency"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.boolean  "is_free",           :default => false
    t.integer  "position"
    t.integer  "plan_list_id"
    t.text     "features_list"
    t.integer  "trial_period_days"
    t.integer  "number_of_embeds"
    t.integer  "number_of_users"
    t.boolean  "has_branding"
    t.boolean  "has_ads"
    t.boolean  "can_add_cta"
    t.integer  "file_size_limit"
    t.integer  "number_of_jobs",    :default => 0
  end

  create_table "posts", :force => true do |t|
    t.string   "title"
    t.text     "body"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "projects", :force => true do |t|
    t.integer  "account_id"
    t.string   "title"
    t.string   "file"
    t.boolean  "file_processing"
    t.hstore   "flowplayer"
    t.string   "allowed_urls"
    t.string   "tags"
    t.string   "analytics"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.string   "license_key"
    t.boolean  "active"
    t.string   "logo_url"
    t.integer  "folder_id"
    t.string   "cid"
    t.text     "embed_css"
    t.integer  "position"
    t.boolean  "favorite",        :default => false
    t.text     "responder_code"
    t.boolean  "dashboard",       :default => false
    t.text     "logged_sites",    :default => ""
  end

  add_index "projects", ["account_id"], :name => "index_projects_on_company_id"

  create_table "redactor_assets", :force => true do |t|
    t.integer  "user_id"
    t.string   "data_file_name",                  :null => false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    :limit => 30
    t.string   "type",              :limit => 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "redactor_assets", ["assetable_type", "assetable_id"], :name => "idx_redactor_assetable"
  add_index "redactor_assets", ["assetable_type", "type", "assetable_id"], :name => "idx_redactor_assetable_type"

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "subscriptions", :force => true do |t|
    t.integer  "account_id"
    t.integer  "plan_id"
    t.string   "customer_token"
    t.datetime "start"
    t.datetime "canceled_at"
    t.string   "status",         :default => "disabled"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  create_table "transactions", :force => true do |t|
    t.integer  "account_id"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "amount"
    t.string   "charge_id"
  end

  create_table "users", :force => true do |t|
    t.integer  "account_id"
    t.string   "name",                   :default => "",    :null => false
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.boolean  "admin",                  :default => false
    t.string   "customer"
    t.string   "last4"
    t.datetime "last_readed_post"
    t.boolean  "owner"
    t.string   "url"
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["unlock_token"], :name => "index_users_on_unlock_token", :unique => true

  create_table "videopages", :force => true do |t|
    t.integer  "project_id"
    t.integer  "template"
    t.text     "widgets"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.hstore   "settings"
    t.text     "slug"
    t.integer  "user_id"
  end

end
