class CreateAddonsPricelists < ActiveRecord::Migration
  def change
    create_table :addons_pricelists do |t|
      t.integer :custom_skin, default: 0
      t.integer :video_importing, default: 0
      t.integer :screenshare_support, default: 0
      t.integer :pay_as_you_go, default: 0

      t.boolean :is_singleton, default: true

      t.timestamps
    end
    add_index(:addons_pricelists, :is_singleton, unique: true)
  end
end
