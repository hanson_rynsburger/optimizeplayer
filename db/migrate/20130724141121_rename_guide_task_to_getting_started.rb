class RenameGuideTaskToGettingStarted < ActiveRecord::Migration
  def up
    drop_table :guide_tasks
    create_table :getting_starteds do |t|
      t.string :title
      t.text :short_description
      t.string :url

      t.timestamps
    end

  drop_table :accounts_guide_tasks
    create_table :accounts_getting_starteds do |t|
      t.integer :account_id
      t.integer :getting_started_id
    end
  end

  def down
    drop_table :accounts_getting_starteds

    create_table :accounts_guide_tasks do |t|
      t.integer :account_id
      t.integer :getting_started_id
    end

    drop_table :getting_starteds

    create_table :guide_tasks do |t|
      t.string :title
      t.text :short_description
      t.string :url

      t.timestamps
    end
  end
end
