class AddUseCloudfrontToCsn < ActiveRecord::Migration
  def change
    add_column :csns, :use_cloudfront, :bool, default: false
  end
end
