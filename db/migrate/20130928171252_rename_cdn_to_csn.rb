class RenameCdnToCsn < ActiveRecord::Migration
  def up
    rename_table :cdns, :csns
    rename_column :assets, :cdn_id, :csn_id
  end

  def down
    rename_table :csns, :cdns
    rename_column :assets, :csn_id, :cdn_id
  end
end
