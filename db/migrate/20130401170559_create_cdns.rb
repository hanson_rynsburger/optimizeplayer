class CreateCdns < ActiveRecord::Migration
  def change
    create_table :cdns do |t|
      t.references :company
      t.string :provider
      t.string :access_key
      t.string :secret_key
      t.string :bucket
      t.boolean :active

      t.timestamps
    end
    add_index :cdns, :company_id
  end
end
