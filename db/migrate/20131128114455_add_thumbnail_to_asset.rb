class AddThumbnailToAsset < ActiveRecord::Migration
  def change
    add_column :assets, :thumbnail, :string
  end
end
