class AddFieldsToCsns < ActiveRecord::Migration
  def change
    add_column :csns, :initialized, :boolean, default: false
    add_column :csns, :rs_temp_url_key,   :string
    add_column :csns, :rs_endpoint_url,   :string
    add_column :csns, :rs_endpoint_path,  :string
  end
end
