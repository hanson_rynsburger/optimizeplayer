Pixelserve::Application.routes.draw do
  mount RedactorRails::Engine => '/redactor_rails'
  mount StripeEvent::Engine => '/stripe'

  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :projects do
        member do
          put 'set_position'
          put 'toggle_favorite'
          put 'toggle_dashboard'
        end
        collection do
          put 'move'
        end
        resources :ctas
      end
      resources :videopages do
        member do
          get 'page'
          put 'save'
          put 'slug_save'
        end
      end
      resources :csns
      resources :assets do
        collection do
          get 'rackspace_streaming_url'
          get "upload_form_params"
        end
        member do
          get "copy_to_cloudfront"
          get "download_progress"
          get "encode_progress"
        end
      end
      resources :plans
      resources :folders
    end
  end

  namespace :account do
    root to: 'main#index'

    post 'create_customer' => "billing#create_customer"

    post "update_card" => "transactions#update_card"
    post "update_plan" => "transactions#update_plan"
    post "buy_addon" => "transactions#buy_addon"

    post "apply_coupon" => "subscriptions#apply_coupon"

    get "team" => "team_members#index"

    resources :users
    resources :team_members do
      collection do
        post "invite"
      end
    end
    resources :billing
    resources :subscriptions do
      member do
        get 'cancel'
      end
    end
  end

  resources :csns, only: [:index] do
    member do
      get "directories"
    end
  end

  resources :projects do
    get 'export', on: :collection
    get 'load_player_code'
  end
  resources :videopages do
    get 'preview'
  end
  get '/videopage/:id' => 'videopages#edit'
  # match "/:user_url" => "videopages#real_preview"

  resources :getting_started, only: [:index, :destroy]
  resources :news, only: [:index, :count ,:show] do
    collection do
      get 'count'
    end
  end

  resources :accounts

  match '/news/detail' => 'news#detail'
  match 'dashboard' => 'main#dashboard'
  match 'help' => 'main#help'
  match 'csn_message' => 'main#csn_message'

  # get '/:id' => 'high_voltage/pages#show', :as => :static, :via => :get


  root to: "main#dashboard"

  post "hide_welcome" => "main#hide_welcome"

  devise_for :users, :controllers => {
                                      :registrations => 'registrations',
                                      :passwords => "devise/passwords"
                                     }
  ActiveAdmin.routes(self)
end
